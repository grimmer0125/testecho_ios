//
//  C4miAudioHandler.h
//  Eagle
//
//  Created by Grimmer Kang on 2011/2/16.
//  Copyright 2011 oplink. All rights reserved.
//

/**
 Note: default is PCM
**/

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
//#import "C4miMediaConnectionManager.h"

#import "Novocaine.h"
//#import <AVFoundation/AVFoundation.h>

#define kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER 3

#define AudioSilenceBytesAAC 10 // 50 // //10?ll
//#define AUDIO_UNKNOWN 900
//#define AUDIO_PCM 116 //1
//#define AUDIO_ADPCM 117 //14
//#define AUDIO_PCM_ULAW 0 //14
//#define AUDIO_AAC 119  //15 //
//#define AUDIO_iLBC 120 //16 special
#define AAC_QUEUEBUFFERBYTESIZE 7680
#define ILBC_QUEUEBUFFERBYTESIZE 456

//pcm
#define AudioSampleRate 8000
#define AudioMonoChannel 1
#define AudioBitsPerChannel 16
#define AudioFramesPerPacket 1
#define AudioSilenceBytes 510//會影響暫停長短，太短感受不一定好 2000//50 //20000

#define AudioRingBufferNumber 3
#define kBufferDurationSecondsAAC 0.15 //no use now
#define kBufferDurationSecondsiLBC 0.15
#define kBufferDurationSecondsADPCM 0.5//0.125 1020
#define kBufferDurationSecondsADPCM_Record 0.25 //0.125 1020

#define kBufferDurationSecondsPCM 0.125 
#define CHECKTIMEDURATION 0.1

#define USEAAC TRUE	


#define DEBUGAUDIO YES

#define KEY_FORMAT                                  @"FORMAT"
#define KEY_SAMPLE_RATE                             @"SAMPLE_RATE"
#define KEY_SAMPLE_PRECISION                        @"SAMPLE_PRECISION"
#define KEY_BIT_RATE                                @"BIT_RATE"

typedef enum {
    MEDIATYPE_NONE = -1,
    VIDEO_MJPEG =26,
    VIDEO_H264=100,
    AUDIO_PCM_ULAW= 0,
    AUDIO_PCM =116,
    AUDIO_ADPCM =117,
    //    AUDIO_AAC = 119, no use
    //    AUDIO_iLBC = 120, no use
    VIDEO_H264_AES =121,
    AUDIO_PCM_ULAW_AES=122,
    AUDIO_PCM_AES=124,
    AUDIO_ADPCM_AES=125,
    VIDEO_MJPEG_AES=126,
    AUDIO_UNKNOWN =900, //dummy
} HBR_PayloadType;

@class AudioPackets;

@interface C4miAudioHandler : NSObject {
	
    Novocaine *audioUnitManager;
    
	AudioStreamBasicDescription		recordFormat;
	AudioQueueRef					recordQueue;
	AudioQueueBufferRef				recordBuffers[kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER];
	
	AudioStreamBasicDescription		playFormat;
	AudioQueueRef					playQueue;
	AudioQueueBufferRef				playBuffers[kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER];	
    
    //** AAC/iLBC
	// 
//	int firstBufferStatus;
//	int secondBufferStatus;
//	int thirdBufferStatus;
//    AudioStreamPacketDescription silenceDesc[1]; 
    //**
    
    //**ADPCM
    int adpcm_play_predictor;
    int adpcm_play_step_index;
    int adpcm_play_step;
    //**ADPCM
    
    NSMutableData *recordData;
    
	//** playing
	NSMutableArray *dataBuffer;
	NSMutableData *currentData;
	
	BOOL firstTimeReceiveData;
	BOOL alreadyStartPlayAudio;
	
	int playDataLength;  //size of each playBuffer
    int preDefinePlayDataLength;

	NSLock *playlockObject;
    NSLock *recordlockObject;
    //**

	//**for testing
    NSTimer *checktimer;
    NSData *testAudioData;
    int startDebugMode;

    //**
    
    //sample rate
    int sampleRate;
    int bitRate;
        
    BOOL runningAudioUnit;
    
    dispatch_queue_t audio_record_queue;
    dispatch_queue_t audio_play_queue;
    
    //adpcm part, old way
    int dec_adpcm_pre_sample;//  = 0;
    int dec_adpcm_index;// = 0;    
    int enc_adpcm_pre_sample;//  = 0;
    int enc_adpcm_index;// = 0;
    
@public

    NSMutableData *testcurrentData;
    NSMutableData *testcurrentData2;

    NSMutableArray *__weak testBuffer;
    NSMutableArray *__weak testBuffer2;
    int typeOfRecordAudioCodec;
    int typeOfPlayAudioCodec;
    int typeOfPrePlayAudioCodec;
    BOOL ifTalkAudio;
    int recordSequenceNumber;
	
}
@property (nonatomic, assign) BOOL runningAudioUnit;
@property (nonatomic, assign) BOOL ifUIEnablePlay;
@property (nonatomic, assign) BOOL ifPlayAudio;
@property (nonatomic, assign) BOOL ifRecordAudio;
@property (nonatomic, assign) BOOL ifTalkAudio;

@property (nonatomic,assign) BOOL firstTimeReceiveData;

@property (atomic, strong) NSMutableData *recordData;

@property (atomic, strong) NSMutableData *currentData;
@property (atomic,strong) NSMutableArray *dataBuffer;

@property (nonatomic, assign) int firstBufferStatus;
@property (nonatomic, assign) int secondBufferStatus;
@property (nonatomic, assign) int thirdBufferStatus;

@property (atomic, strong) NSMutableData *testcurrentData;
@property (atomic, strong) NSMutableData *testcurrentData2;

@property (nonatomic, weak)NSMutableArray *testBuffer;
@property (nonatomic, weak)NSMutableArray *testBuffer2;


@property (nonatomic, assign) SystemSoundID soundFileObject;
@property (nonatomic, assign) CFURLRef soundFileURLRef;

//@property (nonatomic, assign) int enquedNumPackets; //for testing
//@property (nonatomic, assign) int recordNumPackets; //for testing

//- (void)audioTalkSwitch:(BOOL)turnON;

//@property (nonatomic, assign) id  delegate;  
//- (void)receiveAudioData:(NSData*)data type:(HBR_PayloadType)audioType;

//- (void)receiveAudioDataPCM:(NSData *)data;
//- (void)receiveAudioDataAACiLBC:(unsigned char *)rawBytes withPayloadLength:(int)bytesRead withNumPacket:(int)numPacket;

- (int)computePlayBufferSize:(float)seconds; //has more meaning for AAC/iLBC
- (int)computeRecordBufferSize:(float)seconds; //has more meaning for AAC/iLBC

- (void)setupRecordAudioFormatPCM;
//- (void)setupRecordAudioFormatAAC;
//- (void)setupRecordAudioFormatiLBC;
//- (void)setupRecordAudioFormatADPCM;
//
//- (void)setupPlayAudioFormat;
- (void)setupDefaultPlayAudioFormatPCM;
//- (void)setupPlayAudioFormatAAC;
//- (void)setupPlayAudioFormatiLBC;
//- (void)setupPlayAudioFormatADPCM;
- (void) setSamplerate:(int)rate;
//- (void)audioPlayOn:(NSMutableDictionary*)descDict possibleRecording:(BOOL)possibleRecord;
- (void)audioPlayOn:(NSMutableDictionary*)decodeDescDict encode:(NSMutableDictionary*)encodeDescDict possibleRecording:(BOOL)possibleRecord;
//- (void)audioPlaySwitch:(BOOL)turnON;
//- (void)audioRecordSwitch:(BOOL)turnON;

//- (void)audioPlayOn:(HBR_PayloadType)payloadType possibleRecording:(BOOL)possibleRecord;
//- (void)audioPlayOn:(AudioStreamBasicDescription*)desc possibleRecording:(BOOL)possibleRecord;
//- (void)audioPlayOn:(NSMutableDictionary*)descDict possibleRecording:(BOOL)possibleRecord;
- (void)audioPlayOff;


//- (void)audioRecordOn:(HBR_PayloadType)payloadType;
//- (void)audioRecordOn:(NSMutableDictionary*)descDict;
- (void)audioRecordOn:(NSMutableDictionary*)encodeDescDict decode:(NSMutableDictionary*)decodeDescDict;

- (void)audioRecordOff;


//- (void)audioRecordAndPlaySwitch:(BOOL)turnON withRecording:(BOOL)recordFunction
//possibleRemoteListening:(BOOL)possibleListen;

- (void)audioUIPlaySwitch:(BOOL)turnON;//listen 1、on变off：no  2、off变on：yes

void GAQBufferCallback(void                    *inUserData,
					   AudioQueueRef	     	  inAQ,
					   AudioQueueBufferRef	  inCompleteAQBuffer) ;


void GAQBufferFillCallback(	void *								inUserData,
						   AudioQueueRef							inAQ,
						   AudioQueueBufferRef					inBuffer,
						   const AudioTimeStamp *				inStartTime,
						   UInt32								inNumPackets,
						   const AudioStreamPacketDescription*	inPacketDesc);

+ (C4miAudioHandler *)instance;

- (void)setupAudioUnitFormats:(NSMutableDictionary*)decodeDescDict encode:(NSMutableDictionary*)
encodeDescDict;


//Thomas L
//For biz audio
@property (nonatomic,assign) BOOL bizTalking;
@property (nonatomic, strong) NSMutableData *testDataSpeexRecorded;

-(BOOL) switchSpeexTalk;
-(void) startSpeexTalk;
-(void) stopSpeexTalk;

//- (void)registerFinishAndStopAudio:(SystemSoundID) soundID;
//- (void)testAudioUnitRecordSwitch:(BOOL)turnON;
//
//- (void)testAudioUnitRecordAndPlaySwitch:(BOOL)turnON withRecording:(BOOL)recordFunction;
//- (void)testAudioUnitPlaySwitch:(BOOL)turnON;

@end
