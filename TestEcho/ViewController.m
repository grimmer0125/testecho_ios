//
//  ViewController.m
//  TestEcho
//
//  Created by Grimmer on 2015/5/30.
//  Copyright (c) 2015年 Grimmer. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)testRecord:(id)sender
{
    if(self.audioHandler == nil)
    {
        self.audioHandler = [C4miAudioHandler instance];
    }
    
    if (self.audioHandler.ifRecordAudio==false) {
        [[self audioHandler] audioRecordOn:nil decode:nil];//audioUnitRecordSwitch:YES];
    }
    else
    {
        [[self audioHandler] audioRecordOff];//testAudioUnitRecordSwitch:NO];
        
    }
}

- (IBAction)testRecordAndPlay:(id)sender;
{
    if(self.audioHandler == nil)
    {
        self.audioHandler = [C4miAudioHandler instance];
    }
    
    if (self.audioHandler.ifRecordAudio==false)
    {
        [self.audioHandler audioRecordOn:nil decode:nil];//AUDIO_PCM];
        [self.audioHandler audioPlayOn:nil encode:nil possibleRecording:YES];
        
        
        //        [self.audioHandler audioRecordAndPlaySwitch:YES withRecording:YES possibleRemoteListening:YES]; //AudioUnitRecordAndPlaySwitch:YES withRecording:YES];
    }
    else
    {
        [self.audioHandler audioRecordOff];
        [self.audioHandler audioPlayOff];
        
        
        //        [self.audioHandler audioRecordAndPlaySwitch:NO withRecording:NO possibleRemoteListening:false];
        //        [[self audioHandler] audioRecordOff];
    }
    
}

- (IBAction)testPlay:(id)sender
{
    if(self.audioHandler == nil)
    {
        self.audioHandler = [C4miAudioHandler instance];
    }
    
    if (self.audioHandler.ifPlayAudio==false) {
        [[self audioHandler] audioPlayOn:nil encode:nil possibleRecording:false];
        //        [[self audioHandler] audioPlayOn:AUDIO_PCM]; //testAudioUnitPlaySwitch:YES];
    }
    else
    {
        [[self audioHandler] audioPlayOff]; //testAudioUnitPlaySwitch:NO];
        
    }
}


@end
