//
//  AppDelegate.h
//  TestEcho
//
//  Created by Grimmer on 2015/5/30.
//  Copyright (c) 2015年 Grimmer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

