//
//  C4miAudioHandler.m
//  Eagle
//
//  Created by Grimmer Kang on 2011/2/16.
//  Copyright 2011 oplink. All rights reserved.
//

#import "C4miAudioHandler.h"

#import <AVFoundation/AVAudioSession.h>
//#import "C4miRTPGeneralReassembly.h"
//#import "g726codec.h"



#if DEBUGAUDIO
#undef AudioRingBufferNumber
#define AudioRingBufferNumber 100000
#endif

#define ENABLEAUDIOUNIT YES

#define IMA4_PAYLOAD_SIZE 32
#define IMA4_HEADER_SIZE 2

#define BIT_RATE 2
#define DEFAULT_ADPCM_BITRATE 16 // 8000 * 2 * 1/1000


@implementation C4miAudioHandler

//@synthesize delegate;

@synthesize firstTimeReceiveData;

// for play
@synthesize currentData;
@synthesize dataBuffer;
@synthesize ifPlayAudio;

@synthesize ifRecordAudio;

@synthesize ifTalkAudio;

@synthesize ifUIEnablePlay;

//AAC/iLBC
//@synthesize firstBufferStatus;
//@synthesize secondBufferStatus;
//@synthesize thirdBufferStatus;

@synthesize testBuffer;
@synthesize testBuffer2;
@synthesize testcurrentData,testcurrentData2;

@synthesize soundFileObject;
@synthesize soundFileURLRef;

@synthesize runningAudioUnit;
@synthesize recordData;

@synthesize testDataSpeexRecorded = _testDataSpeexRecorded;
@synthesize bizTalking = _bizTalking;


//g726_state_t *g_state;
//static uint8_t rawstring[AudioSilenceBytesAAC]={0};

//static uint8_t ima4_emptyHeader[2]={0}; // 0.125s, 8
//int ima_index_table[16] = {
//    -1, -1, -1, -1, 2, 4, 6, 8,
//    -1, -1, -1, -1, 2, 4, 6, 8
//};
//int ima_step_table[89] = {
//    7, 8, 9, 10, 11, 12, 13, 14, 16, 17,
//    19, 21, 23, 25, 28, 31, 34, 37, 41, 45,
//    50, 55, 60, 66, 73, 80, 88, 97, 107, 118,
//    130, 143, 157, 173, 190, 209, 230, 253, 279, 307,
//    337, 371, 408, 449, 494, 544, 598, 658, 724, 796,
//    876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066,
//    2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358,
//    5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899,
//    15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767
//};

static C4miAudioHandler *audioInstance = nil;

+ (C4miAudioHandler *)instance  {
	
    if(!audioInstance)
    {
        @synchronized(self) {
            if(!audioInstance) {
                audioInstance = [[C4miAudioHandler alloc] init];
            }
        }
    }
    
    return audioInstance;
}

//void playSoundFinished (SystemSoundID sound, void *data) {
//    
//    NSLog(@"resume playSound");
//
//    if ([C4miAudioHandler instance].runningAudioUnit)//runningAudioUnit)//ifRecordAudio)
//    {
////        dispatch_block_t block = ^() {
//
////            [[Novocaine instance] start];
////        };
////        
////        dispatch_async([C4miAudioHandler instance]->audio_play_queue, block);
//    }
//}

//- (void)registerFinishAndStopAudio:(SystemSoundID) soundID;
//{
//    AudioServicesAddSystemSoundCompletion(soundID, nil, nil, playSoundFinished, (void*) self);
//    
//    if(ENABLEAUDIOUNIT && runningAudioUnit)
//    {        
////        [[Novocaine instance] pause];
//    }
//}

- (id)init
{
	self = [super init];
	
    if (self)
    {
        runningAudioUnit=true;
        
        NSURL *tapSound   = [[NSBundle mainBundle] URLForResource: @"APNSSound" withExtension:@"caf"];
        self.soundFileURLRef = (__bridge CFURLRef)tapSound;
        // Create a system sound object representing the sound file.
        AudioServicesCreateSystemSoundID (soundFileURLRef,&soundFileObject);
        
        ifPlayAudio=FALSE;
        ifRecordAudio = FALSE;
        ifTalkAudio=FALSE;
        ifUIEnablePlay=false;
        
        typeOfRecordAudioCodec =AUDIO_UNKNOWN;
        
        typeOfPlayAudioCodec =AUDIO_UNKNOWN;
        typeOfPrePlayAudioCodec = AUDIO_UNKNOWN;
        
        alreadyStartPlayAudio =NO;
		
        playlockObject= [[NSLock alloc] init];
        recordlockObject = [[NSLock alloc] init];
        
        adpcm_play_predictor=0;
        adpcm_play_step_index=0;
        adpcm_play_step=7;
        
        recordSequenceNumber=0;
        
        sampleRate = AudioSampleRate;
        
        audio_record_queue= dispatch_queue_create("audio_record_serical_Queue", NULL);
        audio_play_queue= dispatch_queue_create("audio_play_serical_Queue", NULL);
        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(getAudioData:)
//                                                     name:C4MI_NOTIFY_RECEIVEAUDIODATA
//                                                   object:nil];
        
        startDebugMode=0;
        
        if (currentData==nil) {
            NSMutableData *tmpData = [[NSMutableData alloc] init];
            self.currentData = tmpData;
        }
        
        if (recordData==nil) {
            NSMutableData *tmpData = [[NSMutableData alloc] init];
            self.recordData = tmpData;
        }
        
        if (dataBuffer==nil)
        {
            NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
            self.dataBuffer = tmpArray;
        }
        
        if (self.testDataSpeexRecorded == nil) {
            NSMutableData * tmpData = [[NSMutableData alloc] init];
            self.testDataSpeexRecorded = tmpData;
        }
        
//        initG711u();
//        [self initG726];
//        [self initG726_Encode];
    }
	
	return self;
	
}



//- (void)getAudioData:(NSNotification *)notification
//{
//    NSDictionary *userInfo = [notification userInfo];
//
//    NSData *audioData = [userInfo objectForKey:C4MI_MEDIA];
//
//    HBR_PayloadType mediaType = [[userInfo objectForKey:C4MI_TYPE] intValue];
//    
//    if(self.ifPlayAudio == true /*&& self.audioHandler.ifRecordAudio==false*/) {
//        [self receiveAudioData:audioData type:mediaType];
//    }
//}

- (void)dealloc {
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:C4MI_NOTIFY_RECEIVEAUDIODATA
//                                                  object:nil];
    
	if (checktimer !=nil) {
		[checktimer invalidate];
	}
		
//	if (playQueue != nil)
//	{
    
    if(ifPlayAudio==true)
    {
        if (playQueue != nil && playQueue)
        {
            dispatch_block_t block = ^() {
                
                AudioQueueStop(playQueue, true);
                AudioQueueDispose(playQueue, true);
            };
            
            dispatch_async(audio_play_queue, block);
        }

    }
    

	
	if (playlockObject !=nil) {

//		[playlockObject unlock];
        playlockObject=nil;

	}
    
    if(ifRecordAudio==true)
    {
        dispatch_block_t block = ^() {
            
            [recordlockObject lock];
            
            //        if (recordQueue)
            AudioQueueStop(recordQueue, true);
            AudioQueueDispose(recordQueue, true);
            
            //        if (recordQueue)
            //        {
            //        AudioQueueDispose(recordQueue, true);
            //        recordQueue = nil;
            
            //        }
            
            [recordlockObject unlock];
//            [recordlockObject release];

        };
        
        dispatch_async(audio_record_queue, block);
    }
    else
    {
//        if (recordlockObject !=nil) {
            
//            [recordlockObject unlock];
//        }
    }
    
	
    
    
    
}

- (void)audioUIPlaySwitch:(BOOL)turnON;
{
    ifUIEnablePlay=turnON;
}

//- (void)audioTalkSwitch:(BOOL)turnON {
//
//    if (turnON == ifTalkAudio) {
//        return;
//    }
//    
//	if (turnON==false)
//    {
//        ifTalkAudio=FALSE;
//    }
//    else
//    {
//        ifTalkAudio=TRUE;
//
//    }
//}


- (void)receiveAudioData:(NSData*)data type:(HBR_PayloadType)audioType
{
    if(ifPlayAudio==false) // || ifUIEnablePlay ==false)
    {
        NSLog(@"receiveAudioData but ifplayAudio is false");

        return;
    }
    
    if(true)//ENABLEAUDIOUNIT)// && runningAudioUnit)//[Novocaine instance].playing)
    {
        if(ifPlayAudio)
        {
            //if more than 16K
            [playlockObject lock];
            
            if (audioType == AUDIO_PCM_ULAW || audioType == AUDIO_PCM_ULAW_AES) {
//                char *dst = malloc(data.length*2);
//                Decodepcmu((unsigned char *)data.bytes, (int)data.length, (unsigned char*)dst);
//                NSData *testData = [[NSData alloc] initWithBytes:dst length:data.length*2];
//                
//                [self.currentData appendData:testData];
//                free(dst);
                
            }else if(audioType==AUDIO_PCM || audioType==AUDIO_PCM_AES){
                //bitRate = 128
                [self.currentData appendData:data];
            }
            else if(audioType==AUDIO_ADPCM || audioType==AUDIO_ADPCM_AES)
            {
//                if(bitRate == 0)
//                {
//                    bitRate = DEFAULT_ADPCM_BITRATE;
//                }
//                int _bitRate = bitRate*1000/playFormat.mSampleRate;
//                char *dst = malloc(data.length*16/_bitRate);
//           
//                int ret = 0;
//                ret = G726_2PCM( _bitRate, (g726_state_t *)g_state, (char *)data.bytes, (char *)dst, (int)data.length, 0 );
//                if(ret > 0)
//                {
//                    NSData *testData = [[NSData alloc] initWithBytes:dst length:ret*2];
//                    [self.currentData appendData:testData];
//                }
//                
//                free(dst);
            }
            
            //[self.currentData appendData:data];
            //NSLog(@"%@", self.currentData);
            //DoConvertFile((SInt16*)self.currentData.bytes, [self.currentData length]);
            //NSLog(@"%@",self.currentData);
            
            if (self.currentData.length>16000)
            {
                NSInteger overLen = self.currentData.length-16000;
                
                NSData* nextData = [self.currentData subdataWithRange:NSMakeRange(overLen, self.currentData.length - overLen)];
                
                [self.currentData setData:nextData];
                
            }
                        
            [playlockObject unlock];

        }
    }
//    else
//    {
//        [self receiveAudioQueueData:data type:audioType];
//    }
}

//- (void)testAudioEnque
//{    
//    if(startDebugMode==1)//self->testBuffer.count>0)
//    {
//        if (self->testBuffer!=nil)
//        {
//            if (self->testBuffer.count>0)
//            {
//                HBRLog(@"run the check timer");
//                
//                NSData *tmpData = [self->testBuffer objectAtIndex:0];
//                
//                [self receiveAudioData:tmpData type:AUDIO_ADPCM];
//                
//                [self->testBuffer removeObjectAtIndex:0];
//                
//            }
//            
//        }
//    }
//    else
//    {
//        if (self->testBuffer2!=nil)
//        {
//            if (self->testBuffer2.count>0)
//            {
//                HBRLog(@"run the check timer");
//                
//                
//                NSData *tmpData = [self->testBuffer2 objectAtIndex:0];
//                
//                //                static int count =0;
//                //                count++;
//                
//                //                if (count<200 || count>250) {
//                [self receiveAudioData:tmpData type:AUDIO_ADPCM];
//                //                }
//                
//                [self->testBuffer2 removeObjectAtIndex:0];
//                
//                
//            }
//            
//        }
//    }
//    
//}


//- (void)receiveAudioQueueData:(NSData*)data type:(HBR_PayloadType)audioType
//{
//    if (typeOfPlayAudioCodec ==AUDIO_UNKNOWN)
//    {
//        typeOfPlayAudioCodec=audioType;
//        if (typeOfPlayAudioCodec !=typeOfPrePlayAudioCodec ) {
//            
//            if (typeOfPrePlayAudioCodec !=AUDIO_UNKNOWN) {
//                
//                dispatch_block_t block = ^() {
//                    AudioQueueDispose(playQueue, true);
//                    
//                    [self setupPlayAudioFormat];
//                    
//                };
//                
//                dispatch_async(audio_play_queue, block);
//            }
//            else
//            {
//                dispatch_block_t block = ^() {
//                    
//                    [self setupPlayAudioFormat];
//                    
//                };
//                
//                dispatch_async(audio_play_queue, block);
//            }
//            
//            typeOfPrePlayAudioCodec = typeOfPlayAudioCodec;
//        }
//        
//        int frameDataSize = [data length];
//        
//        switch (typeOfPlayAudioCodec) {
//            case AUDIO_ADPCM:
//            {
//                
//                int adjustPreDefineSize = IMA4_PAYLOAD_SIZE*(preDefinePlayDataLength/IMA4_PAYLOAD_SIZE); //變小
//                
//                //                playDataLength= adjustPreDefineSize>frameDataSize?frameDataSize:adjustPreDefineSize;
//                
//                playDataLength=960;
//                
//                LBRGLog(@"adjsutPredefinedSize:%d",adjustPreDefineSize);
//                
//                break;
//            }
//            case AUDIO_PCM_ULAW:
//            {
//                playDataLength= 3840;
//                break;
//            }
//            case AUDIO_iLBC:
//                playDataLength=preDefinePlayDataLength>frameDataSize?frameDataSize:preDefinePlayDataLength;
//                //[self computePlayBufferSize:kBufferDurationSecondsiLBC];
//                break;
//            case AUDIO_PCM:
//                //                playDataLength=preDefinPlayDataLength>frameDataSize?frameDataSize:preDefinPlayDataLength;
//                
//                playDataLength= 3840;//preDefinePlayDataLength;
//                
//                break;
//            default:
//                break;
//        }
//        
//        
//        HBRLog(@"setup play datalengthplay:%d",playDataLength);
//    }
//    
//    [playlockObject lock];
//    
//    [currentData appendData:data];
//    
//    int currentDataLength = [currentData length];
//    int startPosition = 0;
//    
//    if (currentDataLength>=playDataLength)
//    {
//        const unsigned char *rawBytes = [currentData bytes];
//        
//        while ((currentDataLength-startPosition)>= playDataLength)
//        {
//            NSData *tmpData;
//            
//            if (typeOfPlayAudioCodec==AUDIO_ADPCM)
//            {
//                NSData *preTmpData = [[NSData alloc] initWithBytes:&rawBytes[startPosition] length:playDataLength];
//                
//                HBRLog(@"adpcm old size:%d",[preTmpData length]);
//                
//                tmpData= [self dataFromRawADPCMtoIMA4:preTmpData];
//                
//                HBRLog(@"adpcm new size:%d",[tmpData length]);
//                
//                [preTmpData release];
//            }
//            else
//            {
//                tmpData = [[NSData alloc] initWithBytes:&rawBytes[startPosition] length:playDataLength];
//            }
//            
//            //save the data into the audio queue
//            //HBRLog(@"save the data into the audio queue");
//            if(ifPlayAudio)
//            {
//                if ([dataBuffer count]>=AudioRingBufferNumber) {
//                    [dataBuffer removeObjectAtIndex:0];
//                    HBRLog(@"trim the audio buffer");
//                }
//                [dataBuffer addObject:tmpData];
//            }
//            [tmpData release];
//            
//            startPosition+=playDataLength;
//        }
//        
//        int remainingLength = currentDataLength-startPosition;
//        if( remainingLength>0)
//        {
//            //有剩
//            NSData *remainData= [[NSData alloc] initWithBytes:&rawBytes[startPosition] length:remainingLength];
//            [currentData setData:remainData];
//            [remainData release];
//        }
//        else
//        {
//            
//            [currentData setLength:0];
//        }
//    }
//    
//    [playlockObject unlock];
//    
//    BOOL needStartAudio =false;
//    
//    NSMutableArray *copyBuffer = nil;
//    
//    [playlockObject lock];
//    if ( [dataBuffer count] >= 3  && alreadyStartPlayAudio==NO)
//    {
//        needStartAudio =true;
//        
//        copyBuffer =[[NSMutableArray alloc] initWithArray:dataBuffer copyItems:YES];
//    }
//    
//    [playlockObject unlock];
//    
//    if (needStartAudio)
//    {
//        alreadyStartPlayAudio = YES;
//        
//        HBRLog(@"trigger playing queue 0");
//        
//        dispatch_block_t block = ^() {
//            
//            [playlockObject lock];
//            
//            for (int i=0;i<kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER;i++)
//            {
//                
//                NSData *tmpData = [copyBuffer objectAtIndex:0];
//                int length = [tmpData length];
//                
//                HBRLog(@"manually length:%d",length);
//                
//                memcpy(playBuffers[i]->mAudioData, [tmpData bytes] ,length);
//                
//                playBuffers[i]->mAudioDataByteSize = length;
//                
//                [copyBuffer removeObjectAtIndex:0];
//                
//                
//                AudioQueueEnqueueBuffer(playQueue,playBuffers[i],0,NULL);
//                
//                
//            }
//            
//            [playlockObject unlock];
//            
//            HBRRedLog(@"trigger playing queue 1");
//            
//            if (ifPlayAudio) {
//                
//                //[[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
//                
//                HBRLog(@"audio play start 1");
//                
//                AudioQueueStart(playQueue, NULL);
//                
//                HBRLog(@"audio play start 1*");
//                
//            }
//            
//            HBRLog(@"trigger playing queue 3");
//            
//        };
//        
//        dispatch_async(audio_play_queue, block);
//        
//        [copyBuffer release];
//    }
//}


//34-32 per packet
//- (NSMutableData*)dataFromIMA4toRawADPCM:(NSData*)rawData
//{
//    NSMutableData *nextData=nil;
//    
//    int packetSize = IMA4_PAYLOAD_SIZE+IMA4_HEADER_SIZE;
//    
//    //幾個packets
//    int j_max= [rawData length]/(packetSize);
//    
//    uint8_t *originByte =(uint8_t*)[rawData bytes];
//    
//    for (int j=0; j<j_max; j++)
//    {
//        //2~33
//        for (int k=2; k<packetSize; k++)
//        {
//            //exchange upper 4bit and lower 4 bit
//            uint8_t tmp = originByte[k+packetSize*j]<<4 | originByte[k+packetSize*j]>>4;
//            
//            if (nextData==nil)
//            {
//                nextData= [[NSMutableData alloc] initWithBytes:&tmp length:1];// dataWithBytes:&tmp length:1];
//            }
//            else
//            {
//                [nextData appendBytes:&tmp length:1];
//            }
//        }
//    }
//    
//    return nextData;// autorelease];
//
//}

//32->34 per packet
//- (NSData*)dataFromRawADPCMtoIMA4:(NSData*)rawData
//{
//    NSMutableData *nextData=nil;
//        
//    int j_max= [rawData length]/IMA4_PAYLOAD_SIZE;
//    
//    uint8_t *originByte =(uint8_t*)[rawData bytes];
//    
//    //30個 32byte(packet)
//    for (int j=0; j<j_max; j++)
//    {        
//        if (nextData==nil)
//        {
//            nextData= [[NSMutableData alloc] initWithBytes:ima4_emptyHeader length:2];
//        }
//        else
//        {
//            uint8_t firstByte= (adpcm_play_predictor>1) & 0xff;
//            [nextData appendBytes:&firstByte length:1];
//            
//            uint8_t secondByte = (adpcm_play_predictor<<7) | adpcm_play_step_index;
//            [nextData appendBytes:&secondByte length:1];
//            
//        }
//        
//        for (int k=0; k<IMA4_PAYLOAD_SIZE; k++) {
//            
//            uint8_t tmp = originByte[k+IMA4_PAYLOAD_SIZE*j]<<4 | originByte[k+IMA4_PAYLOAD_SIZE*j]>>4;
//                        
//            [nextData appendBytes:&tmp length:1];
//            
//            
//            int lower_nibble = tmp & 0x0F;
//            int upper_nibble = (tmp & 0xF0) >> 4;
//            
//            // Decode the lower nibble
//            adpcm_play_step_index = adpcm_play_step_index + ima_index_table[(unsigned)lower_nibble];
//            int diff = ((signed)lower_nibble + 0.5) * adpcm_play_step / 4;
//            adpcm_play_predictor += diff;
//            if (adpcm_play_step_index<0) {
//                adpcm_play_step_index=0;
//            }
//            else if(adpcm_play_step_index>88)
//            {
//                adpcm_play_step_index=88;
//            }
//            if (adpcm_play_predictor<-32768)
//            {
//                adpcm_play_predictor = -32768;
//            }
//            else if (adpcm_play_predictor>32767)
//            {
//                adpcm_play_predictor  = 32767;
//            }
//            adpcm_play_step = ima_step_table[adpcm_play_step_index];
//            
//            // Decode the uppper nibble
//            adpcm_play_step_index += ima_index_table[(unsigned)upper_nibble];
//            diff = ((signed)upper_nibble + 0.5) * adpcm_play_step / 4;
//            adpcm_play_predictor = adpcm_play_predictor + diff;
//            if (adpcm_play_step_index<0) {
//                adpcm_play_step_index=0;
//            }
//            else if(adpcm_play_step_index>88)
//            {
//                adpcm_play_step_index=88;
//            }
//            if (adpcm_play_predictor<-32768)
//            {
//                adpcm_play_predictor = -32768;
//            }
//            else if (adpcm_play_predictor>32767)
//            {
//                adpcm_play_predictor  = 32767;
//            }
//            adpcm_play_step = ima_step_table[adpcm_play_step_index];
//            
//        }
//        
//    }
//    
//    return nextData;// autorelease];
//    
//}

- (void)audioPlayOn:(NSMutableDictionary*)decodeDescDict encode:(NSMutableDictionary*)encodeDescDict possibleRecording:(BOOL)possibleRecord
{
    if (ifPlayAudio) {
        return;
    }
        
    if(true)
    {
//        runningAudioUnit=true;

        ifPlayAudio=TRUE;
        
//        [self initG726];

        
        //audio unit process
        if(ifRecordAudio) //有人已經在錄聲音了
        {
            //直接設定audio unit record callback

            [[Novocaine instance] setOutputSInt16Block:^(SInt16 *data, UInt32 numFrames, UInt32 numChannels)
             {
                 @autoreleasepool {
                     [self audioUnitPlayCallbackSInt16:data frames:numFrames channels:numChannels];
                 }
             }];
            
            [[Novocaine instance] setOutputBlock:^(float *data, UInt32 numFrames, UInt32 numChannels)
             {
                 @autoreleasepool {

                     [self audioUnitPlayCallback:data frames:numFrames channels:numChannels];
                 }
             }];
        }
        else
        {
            
//            input_speakerFormat.mSampleRate = format.mSampleRate; //44100;
            //            input_speakerFormat.mBitsPerChannel= format.mBitsPerChannel; //16 or 32

//            input_speakerFormat.mBytesPerFrame= format.mBytesPerFrame; //2 or 4
//            input_speakerFormat.mFormatFlags= format.mFormatFlags;
                        
            //sampleRate
            [self setupAudioUnitFormats:decodeDescDict encode:encodeDescDict];
            
            [[Novocaine instance] setOutputSInt16Block:^(SInt16 *data, UInt32 numFrames, UInt32 numChannels)
             {
                 @autoreleasepool {

                     [self audioUnitPlayCallbackSInt16:data frames:numFrames channels:numChannels];
                 }
             }];
            
            [[Novocaine instance] setOutputBlock:^(float *data, UInt32 numFrames, UInt32 numChannels)
             {
                 @autoreleasepool {

                     [self audioUnitPlayCallback:data frames:numFrames channels:numChannels];
                 }
             }];
            
            
            dispatch_block_t block = ^() {
                
                [[Novocaine instance] checkAvailableThenSetup:playFormat encode:recordFormat  possibleRecording:possibleRecord];

                [[Novocaine instance] start];

            };
            
            dispatch_async(audio_play_queue, block);
            
            
        }
    }
//    else
//    {
//        [self audioQueuePlayOn];
//    }    
}

//- (void)audioQueuePlayOn
//{
//    
//    ifPlayAudio=TRUE;
//    
//    //            self.dataBuffer = [NSMutableArray array];
//    
//    //run a testing timer
//    if (DEBUGAUDIO)
//    {
//        //                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"audio" ofType:@"adpcm"];
//        //
//        //                testAudioData= [[NSData alloc] initWithContentsOfFile:filePath];
//        //
//        //                //放進去,
//        //                const int fixedDataLength = 960;
//        //
//        //                int count = [testAudioData length]/fixedDataLength;
//        //
//        //
//        //                //每一次play開始都是新的,
//        //                //以下是很多個960, 所以newDataLength= 930之類的xxxx;
//        //                //  x/930得到幾個i, 然後930/32得到j的上限,每次32的loop
//        //        //            int predictor = 0;
//        //        //            int step_index = 0;
//        //        //            int step = 7;
//        //
//        //                for (int i=0; i<count; i++)
//        //                {
//        //
//        //                    NSData *tmpData =[testAudioData subdataWithRange:NSMakeRange(i*fixedDataLength, fixedDataLength)];
//        //
//        //        //                //每一次960開始, 很多個packet
//        //        //                NSData *ima4data= [self dataFromADPCMtoIMA4:tmpData];
//        //
//        //                    if(self->testBuffer==nil)
//        //                    {
//        //                        self->testBuffer =[[NSMutableArray alloc] initWithCapacity:1];
//        //
//        //                    }
//        //
//        //                    //30 packets
//        //                    [self->testBuffer addObject:tmpData];
//        //
//        //                }
//        
//        startDebugMode++;
//        
//        checktimer = [NSTimer scheduledTimerWithTimeInterval:CHECKTIMEDURATION target:self selector:@selector(testAudioEnque) userInfo:self repeats:YES];
//    }
//}

- (void)audioPlayOff
{
    
    ifPlayAudio=false;

    if(true)//ENABLEAUDIOUNIT)// && runningAudioUnit )
    {                
        //stop audio unit play
        if(ifRecordAudio==false)
        {
//            runningAudioUnit=false;
            
            ifRecordAudio=false;

            dispatch_block_t block = ^() {
                
                [[Novocaine instance] pause];
                [[Novocaine instance] dispoaseAudio];
                
            };
            
            dispatch_async(audio_play_queue, block);
        }
        
        [playlockObject lock];
                
        [currentData setLength:0];
        
        [playlockObject unlock];
        
        
        return;
    }
//    else
//    {
//        //stop audio queue play
////        ifPlayAudio=FALSE;
//        typeOfPlayAudioCodec =AUDIO_UNKNOWN;
//        
//        alreadyStartPlayAudio = NO;
//        
//        if (checktimer !=nil) {
//            [checktimer invalidate];
//            checktimer = nil;
//        }
//                
//        dispatch_block_t block = ^() {
//            
//            HBRLog(@"audio play stop 1");
//            
//            AudioQueueFlush(playQueue);
//            AudioQueueStop(playQueue, true);
//            
//            HBRLog(@"audio play stop 2");
//            
//        };
//        
//        dispatch_async(audio_play_queue, block);
//        
//        [playlockObject lock];
//        
//        if (dataBuffer) {
//            [dataBuffer removeAllObjects];
//        }
//        
//        if (self.currentData) {
//            [currentData setLength:0];
//        }
//        
//        [playlockObject unlock];
//        
//        adpcm_play_predictor=0;
//        adpcm_play_step_index=0;
//        adpcm_play_step=7;
//    }
}

//- (void)setupPlayAudioFormat
//{
//    switch (typeOfPlayAudioCodec) {
//            
//            //            case AUDIO_AAC:
//            //            {
//            //                [self setupPlayAudioFormatAAC];
//            //
//            //                AudioQueueNewOutput(&playformat, GAQBufferCallback,self,NULL, kCFRunLoopCommonModes, 0, &playqueue);
//            //
//            //                int bufferByteSize =[self computeRecordBufferSize:kBufferDurationSecondsAAC];
//            //
//            //                //alloac vbr buffer
//            //                for (int i =0 ;i < kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER;i++)
//            //                {
//            //                    HBRLog(@"allocate adpcm queue buffer");
//            //                    AudioQueueAllocateBuffer(playqueue, bufferByteSize, &playbuffers[i]);
//            //
//            //                }
//            //                break;
//            //            }
//        case AUDIO_ADPCM:
//        {
//            [self setupPlayAudioFormatADPCM];
//            
//            AudioQueueNewOutput(&playFormat, GAQBufferCallback,self,NULL, kCFRunLoopCommonModes, 0, &playQueue);
//            
//            int bufferByteSize =[self computePlayBufferSize:kBufferDurationSecondsADPCM];
//            
//            preDefinePlayDataLength = bufferByteSize;
//            
//            //alloac vbr buffer
//            for (int i =0 ;i < kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER;i++)
//            {
//                HBRRedLog(@"allocate adpcm queue buffer:%d",bufferByteSize);
//                AudioQueueAllocateBuffer(playQueue, bufferByteSize, &playBuffers[i]);
//                
//            }
//            break;
//        }
//        case AUDIO_PCM_ULAW:
//        {
//            [self setupPlayAudioFormatUlaw];
//            
//            AudioQueueNewOutput(&playFormat, GAQBufferCallback,self,NULL, kCFRunLoopCommonModes, 0, &playQueue);
//            
//            int bufferByteSize =3840;//[self computePlayBufferSize:kBufferDurationSecondsPCM];
//            
//            preDefinePlayDataLength = bufferByteSize;
//            
//            for (int i =0 ;i < kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER;i++)
//            {
//                HBRRedLog(@"allocate queue buffer:%d",bufferByteSize);
//                AudioQueueAllocateBuffer(playQueue, bufferByteSize, &playBuffers[i]);
//            }
//            break;
//        }
//        case AUDIO_iLBC:
//        {
//            [self setupPlayAudioFormatiLBC];
//            
//            AudioQueueNewOutput(&playFormat, GAQBufferCallback,self,NULL, kCFRunLoopCommonModes, 0, &playQueue);
//            
//            int bufferByteSize =[self computePlayBufferSize:kBufferDurationSecondsiLBC];
//            
//            preDefinePlayDataLength = bufferByteSize;
//
//            
//            //alloac vbr buffer
//            for (int i =0 ;i < kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER;i++)
//            {
//                HBRLog(@"allocate ilbc queue buffer:%d",bufferByteSize);
//                AudioQueueAllocateBuffer(playQueue, bufferByteSize, &playBuffers[i]);
//                
//            }
//            break;
//        }
//        case AUDIO_PCM:
//        {
//            [self setupPlayAudioFormatPCM];
//            
//            AudioQueueNewOutput(&playFormat, GAQBufferCallback,self,NULL, kCFRunLoopCommonModes, 0, &playQueue);
//            
//            int bufferByteSize =3840;//[self computePlayBufferSize:kBufferDurationSecondsPCM];
//            
//            preDefinePlayDataLength = bufferByteSize;
//
//            for (int i =0 ;i < kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER;i++)
//            {
//                HBRRedLog(@"allocate queue buffer:%d",bufferByteSize);
//                AudioQueueAllocateBuffer(playQueue, bufferByteSize, &playBuffers[i]);	
//            }
//        }
//        default:
//            break;
//    }
//    
//    
//    HBRRedLog(@"status of playformat format:%f,\n%ld,\n%ld,\n%ld,\n%ld,\n%ld,\n%ld,\n%ld,\n%ld",
//		  playFormat.mSampleRate,
//		  playFormat.mFormatID, //?
//		  playFormat.mBytesPerPacket,// ? //2
//		  playFormat.mFramesPerPacket,//? 1
//		  playFormat.mBytesPerFrame, //? shuld 2
//		  playFormat.mChannelsPerFrame, //1
//		  playFormat.mBitsPerChannel, //16
//		  playFormat.mFormatFlags, //12 ???
//		  playFormat.mReserved);
//}

//-(void)setupPlayAudioFormatUlaw
//{
//    playFormat.mSampleRate= 8000;
//    playFormat.mFormatFlags = 0;
//	playFormat.mFormatID= kAudioFormatULaw;
//    
//	playFormat.mFramesPerPacket=1;//
//	playFormat.mChannelsPerFrame =1;
//	playFormat.mBitsPerChannel = 8;//
//	playFormat.mReserved=0;
//    
//    playFormat.mBytesPerFrame= playFormat.mBitsPerChannel * playFormat.mChannelsPerFrame / 8;
//    playFormat.mBytesPerPacket = playFormat.mBytesPerFrame * playFormat.mFramesPerPacket;
//}

-(void)setupDefaultPlayAudioFormatPCM
{
	playFormat.mFormatID = kAudioFormatLinearPCM;
	playFormat.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger;// | kLinearPCMFormatFlagIsPacked;
	
	playFormat.mBitsPerChannel = AudioBitsPerChannel; //16;
	
	//playFormat.mSampleRate =AudioSampleRate; //8000.0;
    playFormat.mSampleRate = sampleRate;
	playFormat.mChannelsPerFrame = AudioMonoChannel; //1;
	
	playFormat.mBytesPerPacket = playFormat.mBytesPerFrame = (playFormat.mBitsPerChannel / 8) * playFormat.mChannelsPerFrame;
	playFormat.mFramesPerPacket = AudioFramesPerPacket;
	
	playFormat.mReserved = 0;
    bitRate = 0;
}

- (void)setSampleRate:(NSInteger)rate
{
    playFormat.mSampleRate = rate;
}

//-(void)setupPlayAudioFormatiLBC
//{
//	
//	playFormat.mSampleRate= 8000;//22050;
//	playFormat.mFormatID= kAudioFormatiLBC; //kAudioFormatMPE G4AAC_LD;
//	playFormat.mBytesPerPacket=38; //!!!
//	playFormat.mFramesPerPacket=160;//
//	playFormat.mBytesPerFrame=0;
//	playFormat.mChannelsPerFrame =1;
//	playFormat.mBitsPerChannel=0;		
//	playFormat.mFormatFlags=0;
//	playFormat.mReserved=0;
//	
//}
//
//-(void)setupPlayAudioFormatADPCM
//{
//	
//	playFormat.mSampleRate= 8000;
//	playFormat.mFormatID= kAudioFormatAppleIMA4;
//    playFormat.mBytesPerPacket=34; //!!!
//	playFormat.mFramesPerPacket=64;//
//	playFormat.mBytesPerFrame=0;
//	playFormat.mChannelsPerFrame =1;
//	playFormat.mBitsPerChannel=0;//
//	playFormat.mFormatFlags=0;
//	playFormat.mReserved=0;
//	
//}

//-(void)setupPlayAudioFormatAAC
//{
//    
//	playformat.mSampleRate= 22050;
//	playformat.mFormatID= kAudioFormatMPEG4AAC_LD;
//	playformat.mBytesPerPacket=0;
//	playformat.mFramesPerPacket=512; // should be 512 !!
//	playformat.mBytesPerFrame=0;
//	playformat.mChannelsPerFrame =1;
//	playformat.mBitsPerChannel=0;
//	playformat.mFormatFlags=0;
//	playformat.mReserved=0;
//	
//}


//void GAQBufferCallback(void                    *inUserData,
//					   AudioQueueRef	     	  inAQ,
//					   AudioQueueBufferRef	  inCompleteAQBuffer) 
//{
////    HBRLog(@"play audio !!!!!!!!!!");
//    
//	C4miAudioHandler *THIS = (C4miAudioHandler *)inUserData;
//
////	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];	
//	
//    //pcm part
////	if ([THIS->dataBuffer count] > 0)
////	{		
////		HBRLog(@"play audio piece");
////        
////		[THIS->lockObject lock];
////		
////		memcpy(inCompleteAQBuffer->mAudioData, [[THIS->dataBuffer objectAtIndex:0] bytes] , THIS->dataLength);
////		[THIS->dataBuffer removeObjectAtIndex:0];
////		
////		[THIS->lockObject unlock];
////		
////		inCompleteAQBuffer->mAudioDataByteSize = THIS->dataLength;
////        
////		if(THIS->ifRunAudio)
////		{
////			AudioQueueEnqueueBuffer(inAQ,inCompleteAQBuffer,0,NULL);
////		}
////        
////	}
////	else 
////	{
////		
////		static uint8_t rawstring[AudioSilenceBytes]={0}; // 0.125s, 8
////        
////		memcpy(inCompleteAQBuffer->mAudioData, rawstring , AudioSilenceBytes);
////		inCompleteAQBuffer->mAudioDataByteSize = AudioSilenceBytes;
////		
////		if(THIS->ifRunAudio) 
////		{
////			AudioQueueEnqueueBuffer(inAQ,inCompleteAQBuffer,0,NULL);
////		}
////		
////		HBRLog(@"no more data");
////        //, orig:%d",[[NSNumber numberWithInt:thisTime] intValue],thisTime);
////
////	}
//    
////    HBRLog(@"test7");
//
//    [THIS->playlockObject lock];
//
//    
//    if ([THIS->dataBuffer count] > 0)
//	{
////		HBRLog(@"play audio piece:%d",[[THIS->dataBuffer objectAtIndex:0] length]);
//		
//        NSData *tmpData = [THIS->dataBuffer objectAtIndex:0];
//        
//        int length = [tmpData length];
//        
////       HBRLog(@"length:%d",length);
//        
////        int playlength = length>1054?1054:length;
////        
////        HBRLog(@"playlength:%d",playlength);
//
////        if (length>1054) {
////            playlengh=
//        
//        
////        }
//        
//        HBRLog(@"play audio !!!!%d",length);
//        
//		memcpy(inCompleteAQBuffer->mAudioData, [tmpData bytes] ,length);
//        
//        inCompleteAQBuffer->mAudioDataByteSize = length;
//
//        [THIS->dataBuffer removeObjectAtIndex:0];
//        		
//		        
//		if(THIS->ifPlayAudio)
//		{
//			AudioQueueEnqueueBuffer(inAQ,inCompleteAQBuffer,0,NULL);
//		}
//        
////        HBRLog(@"play audio end");
//        
//	}
//	else
//	{
//		
//		static uint8_t rawstring[AudioSilenceBytes]={0}; // 0.125s, 8
//        
//		memcpy(inCompleteAQBuffer->mAudioData, rawstring , AudioSilenceBytes);
//		inCompleteAQBuffer->mAudioDataByteSize = AudioSilenceBytes;
//		
//		if(THIS->ifPlayAudio)
//		{
//			AudioQueueEnqueueBuffer(inAQ,inCompleteAQBuffer,0,NULL);
//		}
//		
//        HBRRedLog(@"no more data and use silence data");
//        
//        
//        //, orig:%d",[[NSNumber numberWithInt:thisTime] intValue],thisTime);
//		
//	}
//    
//    [THIS->playlockObject unlock];
//    
////    HBRLog(@"test8");
//
//
//
////	[pool drain];
//}

//no use now, just for testing
- (void)audioRecordAndPlaySwitch:(BOOL)turnON withRecording:(BOOL)recordFunction possibleRemoteListening:(BOOL)possibleListen
{
//    if (!DEBUGAUDIO)
//    {
//        if (turnON && recordFunction) {
//            //enable in the same time
//            [self audioRecordOn:nil];//AUDIO_ADPCM];
//            [self audioPlayOn:nil possibleRecording:possibleListen]; //AUDIO_ADPCM
//            
//            return;
//        }
//    }
    
    if(recordFunction)
    {
        [self audioRecordOn:nil decode:nil];//AUDIO_PCM];
    }
    else
    {
        [self audioRecordOff];
    }
    
    if (turnON) {
        [self audioPlayOn:nil encode:nil possibleRecording:possibleListen];
//        [self audioPlayOn:nil possibleRecording:possibleListen]; //AUDIO_PCM
    }
    else
    {
        [self audioPlayOff];
    }  
}

- (void)audioUnitPlayCallbackSInt16:(SInt16 *)data frames:(UInt32)numFrames channels:(UInt32) numChannels
{
//    NSLog(@"audioUnitPlayCallback");
    
    if(ifPlayAudio)
    {
//        if(true) //!DEBUGAUDIO)
//        {            
        int sizeOfPlay =sizeof(SInt16)*numFrames*numChannels;
                
        [playlockObject lock];
        
        NSMutableData *tmpData=nil;
        if(!DEBUGAUDIO)
        {
            tmpData= self.currentData;
        }
        else
        {
            if (self.testcurrentData !=nil && self.testcurrentData.length>0) 
            {
                tmpData=self.testcurrentData;
                
                int savedDataSize = tmpData.length;
                
                if (sizeOfPlay>savedDataSize) {
                    
                    tmpData=self.testcurrentData2;
                }
            }
            else
            {
                tmpData=self.testcurrentData2;
            }
        }
        
        
        int savedDataSize = (int)tmpData.length;
        
        //            NSData* tmpData = [self.dataBuffer objectAtIndex:0];
        
        //            int sizeOfPlay =sizeof(SInt16)*numFrames*numChannels;
        
        //有放過後若之後沒資料塞會嗡嗡聲,silence data
        //            if(playtimes<=400)
        //            {
        //                playtimes++;
        //
        //                SInt16 *z  = malloc(sizeOfPlay);
        //                memset(z, 0, sizeOfPlay);
        //                memcpy(data, z, sizeOfPlay);
        //                free(z);
        //
        //                [playlockObject unlock];
        //
        //
        //                return;
        //            }
        
        if (tmpData==nil || sizeOfPlay>savedDataSize) {
            //                NSLog(@"size too small !!!!!!!!!!!!!!!!!!");
            
            //一開始有時的爆聲應該不是因為沒塞silence data
            SInt16 *z  = malloc(sizeOfPlay);
            memset(z, 0, sizeOfPlay);
            memcpy(data, z, sizeOfPlay);
            free(z);
            
            
        }
        else if (sizeOfPlay ==savedDataSize)
        {
            
            //                playtimes++;
            
            SInt16 *z = (SInt16*)tmpData.bytes;
            
            //copy右邊到左邊
            memcpy(data, z, savedDataSize);
            
            [tmpData setLength:0];
        }
        else
        {                
            SInt16 *z = (SInt16*)tmpData.bytes;
            //NSLog(@"sizeOfPlay:%d/savedDataSize:%d", sizeOfPlay,savedDataSize);
            
            for (int iChannel=0; iChannel < numChannels; ++iChannel)
            {
                for (int i=0; i < numFrames; ++i)
                {
                    
                    int index = iChannel+i*numChannels;
                    data[index] = z[index];
                }
            }
            
            NSData* nextData = [tmpData subdataWithRange:NSMakeRange(sizeOfPlay, savedDataSize - sizeOfPlay)];
            
            [tmpData setData:nextData];
            
        }
        
        [playlockObject unlock];

    }
    else
    {
        int sizeOfPlay =sizeof(SInt16)*numFrames*numChannels;

        SInt16 *z  = malloc(sizeOfPlay);
        memset(z, 0, sizeOfPlay);
        memcpy(data, z, sizeOfPlay);
        free(z);
    }
    
}

- (void)audioUnitPlayCallback:(float *)data frames:(UInt32)numFrames channels:(UInt32) numChannels

{
    if(ifPlayAudio)
    {
        if(DEBUGAUDIO)
        {
              //play testBuffer
            if (self->testBuffer.count>0)
            {
                NSData *tmpData = [self->testBuffer objectAtIndex:0];
                
                int sizeOfOutput =sizeof(float)*numFrames*numChannels;
                int magnitude = sizeOfOutput/tmpData.length;
                
                if(sizeOfOutput%tmpData.length==0)
                {
                    float *z = (float*)tmpData.bytes;
                    
                    for (int iChannel=0; iChannel < numChannels; ++iChannel)
                    {                    
                        for (int i=0; i < numFrames; ++i)
                        {
                            
                            //e.g. 0,1 是原本的0 ,
                            int index = iChannel+i*numChannels;
                            int rightIndex= index/magnitude;
                            data[index] = z[rightIndex];
                        }
                    }
                }
                
                [self->testBuffer removeObjectAtIndex:0];
                
            }
            else
            {
                //play testBuffer2
                if (self->testBuffer2.count>0)
                {
                    
                    NSData *tmpData = [self->testBuffer2 objectAtIndex:0];
                    
                    int sizeOfOutput =sizeof(float)*numFrames*numChannels;
                    int magnitude = sizeOfOutput/tmpData.length;
                    
                    if(sizeOfOutput%tmpData.length==0)
                    {
                        
                        float *z = (float*)tmpData.bytes; //saved data
                        //                 [tmpData getBytes:&z length:sizeof(float)*numFrames*numChannels];
                        
                        for (int iChannel=0; iChannel < numChannels; ++iChannel)
                        {
                            //                     float *channelStart =  &data[iChannel];
                            //                 &outData[iChannel]
                            
                            for (int i=0; i < numFrames; ++i)
                            {
                                int index = iChannel+i*numChannels;
                                data[index] = z[index/magnitude];
                                
                            }
                        }
                    }
                    
                    [self->testBuffer2 removeObjectAtIndex:0];
                    
                }
            }
        }
        else
        {
        }
    }
}


- (void)audioUnitRecordCallbackSInt16:(SInt16 *)data frames:(UInt32)numFrames channels:(UInt32) numChannels
{
    
    if(ifRecordAudio)
    {
        @autoreleasepool
        {
            if(!DEBUGAUDIO)
            {
                int size = sizeof(SInt16)*numFrames*numChannels;

                char *dst;
                int dstSize= 0;
                
                //應該是不用加aes
                if (typeOfRecordAudioCodec==AUDIO_PCM_ULAW)
                {
//                    dstSize = size/2;
//                    dst = malloc(dstSize);
//                    Encodepcmu((unsigned char *)data, dstSize, (unsigned char*)dst);
//                    
                }
                else if(typeOfRecordAudioCodec==AUDIO_ADPCM)
                {
//                    dstSize=size/8;
//                    dst=malloc(dstSize);
//                    
//                    NSDate* startData = [NSDate date];
//                    PCM2G726( BIT_RATE, (g726_state_t *)g_state, (char *)data, (char *)dst, size, 0 );
//
//                    double deltaTime = [[NSDate date] timeIntervalSinceDate:startData];
//                    NSLog(@"encode %dBytes cost time = %f", size, deltaTime);

                }
                else if (typeOfRecordAudioCodec==AUDIO_PCM)
                {
                    dstSize = size;
                    dst = malloc(dstSize);
                    memset(dst, 0, size);
                    memcpy(dst, data, dstSize);
                }

                else
                {
//                    HBRRedLog(@"no obvious encode format so return");
                    return;
                }
                
                [recordlockObject lock];
                
                [self.recordData appendBytes:dst length:dstSize];
                
                //check and send
                //把aes128/256時需要的16byte倍數資料取出來
                
//                int _bitRate = bitRate*1000/playFormat.mSampleRate;
                //bitRate = 128, pcm, 128kbps,
                //0.1*128*1000/8
                float seconds = 0.2;
//                int _bitRate = bitRate*1000/playFormat.mSampleRate;
                
                //now only g.711, 所以原本是 0.2
                if (self.recordData.length>= seconds*bitRate*125) //1000/8
                {

//                    recordSequenceNumber++;
//                    
//                    NSMutableDictionary *info = [[NSMutableDictionary alloc] initWithCapacity:1];
//                    [info setObject:self.recordData  forKey:C4MI_AUDIO];
//
//                    
//                    NSNumber *seqnumber = [NSNumber numberWithInt:recordSequenceNumber];
//                    [info setObject:seqnumber forKey:C4MI_RTPSEQUENCENUM];
//                    
//                    NSNumber *audioType = [NSNumber numberWithInteger:typeOfRecordAudioCodec];
//                    [info setObject:audioType forKey:C4MI_RTP_PayloadType];
//                    
//                    [[NSNotificationCenter defaultCenter] postNotificationName:C4MI_NOTIFY_POSTMEDIADATA object:nil userInfo:info];
//
//                    [self.recordData setLength:0];

                }
                                
                [recordlockObject unlock];
                
                free(dst);
                
            }
            else
            {
                [playlockObject lock];
                
//                //become g.711
                int size = sizeof(SInt16)*numFrames*numChannels;
//                char *dst = malloc(size/2);
//                Encodepcmu((unsigned char *)data, size/2, (unsigned char*)dst);
                
                if(testcurrentData2==nil)
                {
                    [self.testcurrentData  appendBytes:data length:size];//];
                }
                else
                {
                    [self.testcurrentData2  appendBytes:data length:sizeof(SInt16)*numFrames*numChannels];
                }
                
                [playlockObject unlock];
            }
        }
    }
}

- (void)audioUnitRecordCallback:(float *)data frames:(UInt32)numFrames channels:(UInt32) numChannels
{
    if(ifRecordAudio)
    {
        @autoreleasepool
        {        
            if(!DEBUGAUDIO)
            {

                
            }
            else
            {
                //save float* data to testBuffer1
                if(testBuffer2==nil)
                {
                    // encoding
                    NSMutableData *floatData = [NSMutableData dataWithCapacity:1]; //dataWithCapacity:0];
                    
                    //512 frame * 2byte*2 channel*2
                    [floatData appendBytes:data length:sizeof(float)*numFrames*numChannels];
                    
                    [testBuffer addObject:floatData];
                }
                else
                {
                    //save to testBuffer2
                    NSMutableData *floatData = [NSMutableData dataWithCapacity:1]; //dataWithCapacity:0];
                    
                    [floatData appendBytes:data length:sizeof(float)*numFrames*numChannels];
                    
                    [testBuffer2 addObject:floatData];
                }
            }
        }    
    }
}

- (void)setupAudioUnitFormats:(NSMutableDictionary*)decodeDescDict encode:(NSMutableDictionary*)
encodeDescDict
{
    if (decodeDescDict) {
        playFormat.mSampleRate = [[decodeDescDict objectForKey:KEY_SAMPLE_RATE] intValue]*1000; //8000
        //            playFormat.mBytesPerFrame= [[descDict objectForKey:] intValue];//2
        playFormat.mBitsPerChannel= [[decodeDescDict objectForKey:KEY_SAMPLE_PRECISION] intValue];//16;
        bitRate = [[decodeDescDict objectForKeyedSubscript:KEY_BIT_RATE] intValue];
        
        if (playFormat.mBitsPerChannel ==16) {
            playFormat.mBytesPerFrame = 2;
            playFormat.mFormatFlags = 12;
        }
        else
        {
            playFormat.mBytesPerFrame = 4;
            playFormat.mFormatFlags = 41;
        }
        
    }
    else
    {
        [self setupDefaultPlayAudioFormatPCM];
    }
    
    if(encodeDescDict)
    {
        recordFormat.mSampleRate = [[encodeDescDict objectForKey:KEY_SAMPLE_RATE] intValue]*1000;
        recordFormat.mBitsPerChannel= [[encodeDescDict objectForKey:KEY_SAMPLE_PRECISION] intValue];
        
        if (recordFormat.mBitsPerChannel ==16) {
            recordFormat.mBytesPerFrame = 2;
            recordFormat.mFormatFlags = 12;
        }
        else
        {
            recordFormat.mBytesPerFrame = 4;
            recordFormat.mFormatFlags = 41;
        }
    }
    else
    {
        [self setupRecordAudioFormatPCM];
    }        
}

- (void)audioRecordOn:(NSMutableDictionary*)encodeDescDict decode:(NSMutableDictionary*)decodeDescDict
{
    if (ifRecordAudio) {
        return;
    }
    

    ifRecordAudio=TRUE;
    
    NSString *typeStr = [encodeDescDict objectForKey:KEY_FORMAT];
    
    if (typeStr) {
        int type= [typeStr intValue];
        
        switch (type) {
            case 1:                                    
                typeOfRecordAudioCodec = AUDIO_PCM;
                break;
            case 4:
                typeOfRecordAudioCodec = AUDIO_ADPCM;
                break;
            case 5:
                typeOfRecordAudioCodec =AUDIO_PCM_ULAW;
                break;
            default:
                typeOfRecordAudioCodec =AUDIO_PCM_ULAW;
                break;
        }
    }

    if(DEBUGAUDIO)
    {
        if(testcurrentData==nil)
        {
            testcurrentData = [[NSMutableData alloc] init];
        }
        else
        {
            testcurrentData2 = [[NSMutableData alloc] init];

        }
    }
    
    //default 
    if (typeOfRecordAudioCodec ==AUDIO_UNKNOWN)
    {
        typeOfRecordAudioCodec = AUDIO_PCM;
    }
    
    //audio unit process
    if(ifPlayAudio) //有人已經在放聲音了
    {
        //直接設定audio unit record callback
        [[Novocaine instance] setInputBlock:^(float *data, UInt32 numFrames, UInt32 numChannels)
         {
             @autoreleasepool {
                 [self audioUnitRecordCallback:data frames:numFrames channels:numChannels];
             }
         }];
        
        [[Novocaine instance] setInputSInt16Block:^(SInt16 *data, UInt32 numFrames, UInt32 numChannels)
         {
             @autoreleasepool {

                 [self audioUnitRecordCallbackSInt16:data frames:numFrames channels:numChannels];
             }
         }];
    }
    else
    {
        [self setupAudioUnitFormats:decodeDescDict encode:encodeDescDict];
        
        [[Novocaine instance] setInputBlock:^(float *data, UInt32 numFrames, UInt32 numChannels)
         {
             @autoreleasepool {

                 [self audioUnitRecordCallback:data frames:numFrames channels:numChannels];
             }
         }];
        
        [[Novocaine instance] setInputSInt16Block:^(SInt16 *data, UInt32 numFrames, UInt32 numChannels)
         {
             @autoreleasepool {

                 [self audioUnitRecordCallbackSInt16:data frames:numFrames channels:numChannels];
             }
         }];
        
        dispatch_block_t block = ^() {
            
            [[Novocaine instance] checkAvailableThenSetup:playFormat encode:recordFormat possibleRecording:YES];
            
            [[Novocaine instance] start];
        };
        
        dispatch_async(audio_play_queue, block);            
    }
}

- (void)audioRecordOff
{
    if (ifRecordAudio==false) {
        return;
    }
    
    typeOfRecordAudioCodec =AUDIO_UNKNOWN;
    recordSequenceNumber=0;

    //stop audio unit process
    if(true)//ENABLEAUDIOUNIT)// && runningAudioUnit )
    {
        ifRecordAudio=false;

        if (ifPlayAudio==false)
        {
//            runningAudioUnit=false;

            dispatch_block_t block = ^() {
                
                [[Novocaine instance] pause];
                [[Novocaine instance] dispoaseAudio];
            };
            
            dispatch_async(audio_play_queue, block);
        }
        
        [recordlockObject lock];
        [self.recordData setLength:0];
        [recordlockObject unlock];
        
        return;
    }
//    else
//    {
//        //stop audio queue process
//        ifRecordAudio=false;
//        dispatch_block_t block = ^() {
//            
//            [recordlockObject lock];
//            
//            AudioQueueStop(recordQueue, true);
//            
//            [recordlockObject unlock];
//        };
//        
//        dispatch_async(audio_record_queue, block);
//        
//    }    
}

//- (void)audioQueueRecordOn {
//    
//    static int queueRecordOn = 0;
//    
//    if(queueRecordOn==0)
//    {
//        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
//        
//        //if play and record work in the same time, audio queue need this, otherwise too small volumn
//        UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
//        AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute,
//                                 sizeof(audioRouteOverride),&audioRouteOverride);
//    }
//    queueRecordOn++;
//    
//    if(testBuffer==nil)
//    {
//        testBuffer =[[NSMutableArray alloc] initWithCapacity:1];
//    }
//    else
//    {
//        testBuffer2 =[[NSMutableArray alloc] initWithCapacity:1];
//    }
//    
//    ifRecordAudio=TRUE;
//
//    if (typeOfRecordAudioCodec ==AUDIO_UNKNOWN)
//    {
//        typeOfRecordAudioCodec = AUDIO_ADPCM;
//    
//        switch (typeOfRecordAudioCodec)
//        {
//            case AUDIO_iLBC:
//            {
//                [self setupRecordAudioFormatiLBC];
//                
//                break;
//            }
//            case AUDIO_PCM_ULAW:
//            {
//                [self setupRecordAudioFormatULaw];
//                
//                break;
//            }
//            case AUDIO_PCM:
//            {
//                [self setupRecordAudioFormatPCM];
//                
//                break;
//            }
//            default:
//                [self setupRecordAudioFormatADPCM];
//                break;
//        }
//
//        
//        OSStatus result;
//        
//        int bufferByteSize=0;
//            
//        switch (typeOfRecordAudioCodec) {
//            case AUDIO_ADPCM:
//            {
//                result = AudioQueueNewInput(&recordFormat, GAQBufferFillCallback,self,NULL,kCFRunLoopCommonModes,0, &recordQueue);	
//                
//    //            HBRLog(@"status of recording format:%f,\n%ld,\n%ld,\n%ld,\n%ld,\n%ld,\n%ld,\n%ld,\n%ld",
//    //                  recordFormat.mSampleRate,
//    //                  recordFormat.mFormatID, //?
//    //                  recordFormat.mBytesPerPacket,// ? //2
//    //                  recordFormat.mFramesPerPacket,//? 1
//    //                  recordFormat.mBytesPerFrame, //? shuld 2
//    //                  recordFormat.mChannelsPerFrame, //1
//    //                  recordFormat.mBitsPerChannel, //16
//    //                  recordFormat.mFormatFlags, //12 ???
//    //                  recordFormat.mReserved);
//                            
//                //如果recordFormat沒使用過，則可用這來自動得到format，但若先ilbc再adpcm，則adpc的參數不會跟第一次就是adpcm一樣，或有問題
////    			UInt32 size = sizeof(recordFormat);
////                result= AudioQueueGetProperty(recordQueue, kAudioQueueProperty_StreamDescription,
////                                              &recordFormat, &size);
//                
//                bufferByteSize =[self computeRecordBufferSize:kBufferDurationSecondsADPCM_Record];
//                
//                for (int i = 0; i < kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER; ++i) {
//                    AudioQueueAllocateBuffer(recordQueue, bufferByteSize, &recordBuffers[i]);
//                }
//                
//                break;
//            }
//    //		case AUDIO_AAC:
//    //		{
//    //			//get the extra-info for aac-ld
//    //			UInt32 size = sizeof(recordFormat);
//    //			result= AudioQueueGetProperty(recordQueue, kAudioQueueProperty_StreamDescription,
//    //										  &recordFormat, &size);
//    //			
//    //			bufferByteSize =[self computeRecordBufferSize:kBufferDurationSecondsAAC];
//    //			
//    //			for (int i = 0; i < kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER; ++i) {
//    //				AudioQueueAllocateBuffer(recordQueue, bufferByteSize, &recordBuffers[i]);
//    //				AudioQueueEnqueueBuffer(recordQueue, recordBuffers[i], 0, NULL);
//    //			}
//    //            
//    //			break;
//    //		}
//            case AUDIO_iLBC:
//            {
//                result = AudioQueueNewInput(&recordFormat, GAQBufferFillCallback,self,NULL,kCFRunLoopCommonModes,0, &recordQueue);
//                
//    //			UInt32 size = sizeof(recordFormat);
//    //			result= AudioQueueGetProperty(recordQueue, kAudioQueueProperty_StreamDescription,
//    //										  &recordFormat, &size);
//                
//                bufferByteSize =[self computeRecordBufferSize:kBufferDurationSecondsiLBC];
//                
//                for (int i = 0; i < kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER; ++i) {
//                    AudioQueueAllocateBuffer(recordQueue, bufferByteSize, &recordBuffers[i]);
//                    AudioQueueEnqueueBuffer(recordQueue, recordBuffers[i], 0, NULL);
//                }
//                
//                break;
//            }
//            case AUDIO_PCM_ULAW:
//            {
//                //pcm
//                result=AudioQueueNewInput(&recordFormat,GAQBufferFillCallback,self,NULL,NULL,0, &recordQueue);
//                
//                //			UInt32 size = sizeof(recordFormat);
//                //			result= AudioQueueGetProperty(recordQueue, kAudioQueueProperty_StreamDescription,
//                //										  &recordFormat, &size);
//                
//                bufferByteSize =[self computeRecordBufferSize:kBufferDurationSecondsPCM];
//                
//                for (int i = 0; i < kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER; ++i) {
//                    AudioQueueAllocateBuffer(recordQueue, bufferByteSize, &recordBuffers[i]);
//                    AudioQueueEnqueueBuffer(recordQueue, recordBuffers[i], 0, NULL);
//                }
//                
//                break;
//            }
//            case AUDIO_PCM:
//            {
//                //pcm
//                result=AudioQueueNewInput(&recordFormat,GAQBufferFillCallback,self,NULL,NULL,0, &recordQueue);
//
//    //			UInt32 size = sizeof(recordFormat);
//    //			result= AudioQueueGetProperty(recordQueue, kAudioQueueProperty_StreamDescription,
//    //										  &recordFormat, &size);
//                
//                bufferByteSize =[self computeRecordBufferSize:kBufferDurationSecondsPCM];
//                
//                for (int i = 0; i < kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER; ++i) {
//                    AudioQueueAllocateBuffer(recordQueue, bufferByteSize, &recordBuffers[i]);
//                    AudioQueueEnqueueBuffer(recordQueue, recordBuffers[i], 0, NULL);
//                }
//                
//                break;
//            }
//            default:
//                break;
//        }
//        
//        HBRLog(@"record bufferbytesize %d",bufferByteSize);
//
//        HBRLog(@"status of recording format:%f,\n%ld,\n%ld,\n%ld,\n%ld,\n%ld,\n%ld,\n%ld,\n%ld",
//              recordFormat.mSampleRate,
//              recordFormat.mFormatID, //
//              recordFormat.mBytesPerPacket,//
//              recordFormat.mFramesPerPacket,//
//              recordFormat.mBytesPerFrame, //0
//              recordFormat.mChannelsPerFrame, //1
//              recordFormat.mBitsPerChannel, //0
//              recordFormat.mFormatFlags, //0
//              recordFormat.mReserved);//0
//    }
////    else
////    {
////        for (int i = 0; i < kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER; ++i) {
//////            AudioQueueAllocateBuffer(recordQueue, bufferByteSize, &recordBuffers[i]);
////            AudioQueueEnqueueBuffer(recordQueue, recordBuffers[i], 0, NULL);
////        }
////    }
//    
//    
//    //PCM : 		bufferByteSize =[self computeRecordBufferSize:kBufferDurationSecondsPCM];
//    //0.125(2000x8 for 1s)
//    
//    
//    dispatch_block_t block = ^() {
//
//        HBRLog(@"audio record start 0");
//
//        for (int i = 0; i < kSCN_KEY_AUDIO_QUEUE_BUFFER_NUMBER; ++i) {
//
//            AudioQueueEnqueueBuffer(recordQueue, recordBuffers[i], 0, NULL);
//        }
//
//        
//        HBRLog(@"audio record start 1");
//
//        
//        
//        AudioQueueStart(recordQueue, NULL);
//        
//        HBRLog(@"audio record start 2");
//
//    };
//        
//        
//    dispatch_async(audio_record_queue, block);
//    
//    HBRLog(@"audio record switch 1");
//
//
//}

//- (void)setupRecordAudioFormatULaw {
//	
//    recordFormat.mSampleRate= 8000;
//    recordFormat.mFormatFlags = 0;
//	recordFormat.mFormatID= kAudioFormatULaw;
//    
//	recordFormat.mFramesPerPacket=1;//
//	recordFormat.mChannelsPerFrame =1;
//	recordFormat.mBitsPerChannel = 8;//
//	recordFormat.mReserved=0;
//    
//    recordFormat.mBytesPerFrame= playFormat.mBitsPerChannel * playFormat.mChannelsPerFrame / 8;
//    recordFormat.mBytesPerPacket = playFormat.mBytesPerFrame * playFormat.mFramesPerPacket;
//}

- (void)setupRecordAudioFormatPCM {
	
	recordFormat.mSampleRate = AudioSampleRate; //8000.0;
	recordFormat.mChannelsPerFrame = AudioMonoChannel; //1; //mono channel	
    recordFormat.mFormatID = kAudioFormatLinearPCM;

	recordFormat.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked; //12
	recordFormat.mBitsPerChannel = AudioBitsPerChannel; //16;
    
    
    recordFormat.mBytesPerPacket = recordFormat.mBytesPerFrame = (recordFormat.mBitsPerChannel / 8) * recordFormat.mChannelsPerFrame;
	recordFormat.mFramesPerPacket = 1;
    
//	HBRLog(@"bytesperfame:%d",(int)recordFormat.mBytesPerFrame);
	
}

//- (void)setupRecordAudioFormatADPCM {
//	
////    recordFormat.mSampleRate = 8000;
////	recordFormat.mChannelsPerFrame= AudioMonoChannel;
////	recordFormat.mFormatID = kAudioFormatAppleIMA4;
//    
//    recordFormat.mSampleRate= 8000;
//    recordFormat.mChannelsPerFrame =AudioMonoChannel;
//	recordFormat.mFormatID= kAudioFormatAppleIMA4;
//    recordFormat.mBytesPerPacket=34; //!!! ???
//	recordFormat.mFramesPerPacket=64;//64;// ???
//	recordFormat.mBytesPerFrame=0;
//	recordFormat.mBitsPerChannel=0;
//	recordFormat.mFormatFlags=0;
//	recordFormat.mReserved=0;
//    
//    
////    format.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
////	format.mBitsPerChannel = AudioBitsPerChannel; 
//    
//}

//- (void)setupRecordAudioFormatiLBC {
//	
////    recordFormat.mSampleRate = 8000;
////	recordFormat.mChannelsPerFrame= AudioMonoChannel;
////	recordFormat.mFormatID = kAudioFormatiLBC;
//    
//    recordFormat.mSampleRate= 8000;//22050;
//    recordFormat.mChannelsPerFrame =AudioMonoChannel;
//	recordFormat.mFormatID= kAudioFormatiLBC; //kAudioFormatMPE G4AAC_LD;
//	recordFormat.mBytesPerPacket=38; //!!!
//	recordFormat.mFramesPerPacket=160;//
//	recordFormat.mBytesPerFrame=0;
//	recordFormat.mBitsPerChannel=0;
//	recordFormat.mFormatFlags=0;
//	recordFormat.mReserved=0;
//    
//    
////    1768710755,
////    38,
////    160,
////    0,
////    1,
////    0,
////    0,
////    0
//}

//- (void)setupRecordAudioFormatAAC {
//	
//    recordFormat.mSampleRate = 22050;
//	recordFormat.mChannelsPerFrame= AudioMonoChannel;
//	recordFormat.mFormatID = kAudioFormatMPEG4AAC_LD;
//}

// ____________________________________________________________________________________
// Determine the size, in bytes, of a buffer necessary to represent the supplied number
// of seconds of audio data.
- (int)computeRecordBufferSize:(float)seconds
{
	int packets, frames, bytes = 0;
	
	frames = (int)ceil(seconds * recordFormat.mSampleRate);
	
	if (recordFormat.mBytesPerFrame > 0)
		bytes = frames * recordFormat.mBytesPerFrame;
	else {
		UInt32 maxPacketSize;
		if (recordFormat.mBytesPerPacket > 0)
			maxPacketSize = recordFormat.mBytesPerPacket;	// constant packet size
		else {
			UInt32 propertySize = sizeof(maxPacketSize);
			AudioQueueGetProperty(recordQueue, kAudioQueueProperty_MaximumOutputPacketSize, &maxPacketSize,&propertySize);
		}
		if (recordFormat.mFramesPerPacket > 0)
			packets = frames / recordFormat.mFramesPerPacket;
		else
			packets = frames;	// worst-case scenario: 1 frame in a packet
		if (packets == 0)		// sanity check
			packets = 1;
		bytes = packets * maxPacketSize;
	}
	
	NSLog(@"frame per packet %d",(int)recordFormat.mFramesPerPacket); //ilbc:160, aac-ld:512

	return bytes;
}

- (int)computePlayBufferSize:(float)seconds
{
	int packets, frames, bytes = 0;
	
	frames = (int)ceil(seconds * playFormat.mSampleRate);
	
	if (playFormat.mBytesPerFrame > 0)
		bytes = frames * playFormat.mBytesPerFrame;
	else {
		UInt32 maxPacketSize;
		if (playFormat.mBytesPerPacket > 0)
			maxPacketSize = playFormat.mBytesPerPacket;	// constant packet size
		else {
			UInt32 propertySize = sizeof(maxPacketSize);
			AudioQueueGetProperty(playQueue, kAudioQueueProperty_MaximumOutputPacketSize, &maxPacketSize,&propertySize);
		}
		if (playFormat.mFramesPerPacket > 0)
			packets = frames / playFormat.mFramesPerPacket;
		else
			packets = frames;	// worst-case scenario: 1 frame in a packet
		if (packets == 0)		// sanity check
			packets = 1;
		bytes = packets * maxPacketSize;
	}
	
	NSLog(@"play frame per packet %d",(int)playFormat.mFramesPerPacket); //ilbc:160, aac-ld:512
    
	return bytes;
}

//void GAQBufferFillCallback(	void *								inUserData,
//						   AudioQueueRef							inAQ,
//						   AudioQueueBufferRef					inBuffer,
//						   const AudioTimeStamp *				inStartTime,
//						   UInt32								inNumPackets,
//						   const AudioStreamPacketDescription*	inPacketDesc)
//{
//    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
//
//    
//	NSLog(@"AQ buffr for record, n_packet:%d,size:%ld",(int)inNumPackets,inBuffer->mAudioDataByteSize);
//    
//	    
//	C4miAudioHandler *THIS = (C4miAudioHandler *)inUserData;
//	
//    THIS->recordSequenceNumber++;
//    
//	//send to the contact
////	if (USEAAC) //or ILBC
////	{
//                    
////		[THIS->delegate AudioTransferCallBack:inBuffer->mAudioData
////								  rawDataSize:inBuffer->mAudioDataByteSize
////								   numPackets:inNumPackets
////								 packetsDescs:(AudioStreamPacketDescription*)inPacketDesc];
//    if(inBuffer->mAudioDataByteSize>0)
//    {
//        if (DEBUGAUDIO)
//        {
//                    
//    //        static int recordcount =0;
//    //        recordcount++;
//            
//    //        if(recordcount<10)
//    //        {
//    //            unsigned char* packet =  (unsigned char*)inBuffer->mAudioData;
//    //            for (int j = 0; j<(int)inNumPackets; j++)
//    //            {
//    //                int preamble = (packet[j*34] << 8) | packet[j*34+1];
//    //
//    //                int predictor = preamble && 0xFF80;
//    //                // See [Multimedia Wiki][2]
//    //                int step_index = preamble && 0x007F; 
//                    
//    //                HBRLog(@"pre:%d;step_index:%d",predictor,step_index);
//    //                
//    //                HBRLog(@"%02x;%02x",packet[j*34],packet[j*34+1]);
//    //                
//    //                for (int k=0; k<2; k++) {
//    //                    uint8_t byte1 = packet[j*34+k] & 0x01;
//    //                    uint8_t byte2 = packet[j*34+k] & 0x02;
//    //                    uint8_t byte3 = packet[j*34+k] & 0x04;
//    //                    uint8_t byte4 = packet[j*34+k] & 0x08;
//    //                    uint8_t byte5 = packet[j*34+k] & 0x10;
//    //                    uint8_t byte6 = packet[j*34+k] & 0x20; //32
//    //                    uint8_t byte7 = packet[j*34+k] & 0x40; //64
//    //                    uint8_t byte8 = packet[j*34+k] & 0x80; //64
//    //                    
//    //                    HBRLog(@"recording info");
//    //                    
//    //                    HBRLog(@"%d;%d;%d;%d;%d;%d;%d;%d",byte1,byte2,byte3,byte4,byte5,byte6,byte7,byte8);
//    //                    
//    //                    
//    //                }
//
//    //            }
//    //        }
//            
//            //一分半
//            if(TRUE)//recordcount<450)
//            {
//                NSData *tmpData;
//                
//                if (THIS->typeOfRecordAudioCodec==AUDIO_ADPCM)
//                {
//    //                uint8_t* test =(uint8_t*) inBuffer->mAudioData;
//    //                NSLog(@"1orig pointer:%d",test);
//
//                    NSData *originData = [[NSData alloc] initWithBytes:(unsigned char*)inBuffer->mAudioData length:inBuffer->mAudioDataByteSize];
//                    
//    //                uint8_t *originByte =(uint8_t*)[originData bytes];                
//    //                NSLog(@"2orgin pointer:%d",originByte);
//
//                    
//                    tmpData = [THIS dataFromIMA4toRawADPCM:originData];
//                    
//    //                NSLog(@"record 1:%d",[originData retainCount]);
//                    
//                    [originData release];
//                    
////                    int kkk=0;
//                    
//                    
//
//                    
//    //                NSLog(@"record 2");
//
//                }
//                else
//                {
//                    
//                    
////                    for (int i=0; i<20; i++) {
////                        uint8_t *testAudio = (uint8_t*)inBuffer->mAudioData;
////                        
////                        NSLog(@"audio:%02x",testAudio[i]);
////                    }
//  
//                    tmpData = [[NSData alloc] initWithBytes:(unsigned char*)inBuffer->mAudioData length:inBuffer->mAudioDataByteSize];
//                    
//    //                HBRLog(@"othr type:%d",[tmpData length]);
//
//                }
//                
//                //add to testing buffer
//                
//                if(THIS->testBuffer2==nil)
//                {
//                    [THIS->testBuffer addObject:tmpData];
//                }
//                else
//                {
//                    [THIS->testBuffer2 addObject:tmpData];
//                }
//                
//                [tmpData release];
//                
//
//            }
//        }
//        else if(inNumPackets >0)
//        {
//            
//            NSData *tmpData;
//            
//            
//            [THIS->recordlockObject lock];
//            
//            if (THIS->typeOfRecordAudioCodec==AUDIO_ADPCM)
//            {
//                NSData *originData = [[NSData alloc] initWithBytes:(unsigned char*)inBuffer->mAudioData length:inBuffer->mAudioDataByteSize];
//                
//    //            HBRLog(@"orig:%d",[originData length]);
//                
//                tmpData = [THIS dataFromIMA4toRawADPCM:originData];// retain];
//                
//    //            HBRLog(@"after:%d",[tmpData length]);
//                
//                [originData release];
//            }
//            else
//            {
//
//                tmpData = [[NSData alloc] initWithBytes:(unsigned char*)inBuffer->mAudioData length:inBuffer->mAudioDataByteSize];
//
//            }
//            
//            [THIS->recordlockObject unlock];
//
//            
//    //        if (true)//THIS->ifTalkAudio)
//    //        {
//                //send audio
//            
//            NSMutableDictionary *info = [[NSMutableDictionary alloc] initWithCapacity:1];//dictionaryWithCapacity:2];
//
//            [info setObject:tmpData  forKey:C4MI_AUDIO];            
//            [tmpData release];
//            
//            NSNumber *seqnumber = [NSNumber numberWithInt:THIS->recordSequenceNumber];
//
//            [info setObject:seqnumber forKey:C4MI_RTPSEQUENCENUM];
//            
//            
//            if(THIS->ifUIEnablePlay==false)
//            {
//                [[NSNotificationCenter defaultCenter] postNotificationName:C4MI_NOTIFY_POSTMEDIADATA object:nil userInfo:info];
//            }
//            
//            [info release];
//            
//            //LBRGLog(@"END OF POST");
//                        
//    //        }
//        }
//    }
//    else
//    {
//        NSLog(@"no record data");
//    }
//    
////	}
////	else
////	{
////		NSData *tmpAudioData = [[NSData alloc] initWithBytes:inBuffer->mAudioData length:1000];
////		[THIS->delegate AudioTransferCallBack:tmpAudioData];
////	}
//
//	if (THIS->recordQueue != nil)
//	{
//        if (THIS->ifRecordAudio)
//        {
//            
//            AudioQueueEnqueueBuffer(inAQ,inBuffer,0,NULL);
//        }
////        else
////        {
////            HBRLog(@"send audio close 2 !!!!!");
////
////        }
//        
//	}
//	else {
//        HBRLog(@"record queue is nil");
////		HBRLog(@"send audio close 1 !!!!!");
//	}
//    
//    [pool drain];
//}

- (void) setSamplerate:(int)rate
{
    sampleRate = rate;
}

#pragma mark speex callback

-(BOOL) switchSpeexTalk {
    /*
    if (_bizTalking) {
        [self muteSpeexTalk];
    } else {
        [self startSpeexTalk];
    }
     */
    [self startSpeexTalk];
    _bizTalking = !_bizTalking;
    
    return _bizTalking;
}

-(void) startSpeexTalk {
    
    //Do not start if someone is already recording.
    if (ifRecordAudio==YES) {
        return;
    }
    
    ifRecordAudio=YES;
    
    [[Novocaine instance] setInputSInt16Block:^(SInt16 *data, UInt32 numFrames, UInt32 numChannels)
     {
         @autoreleasepool {
             
             if(_bizTalking) {
                 [recordlockObject lock];
                 
                 [self.testDataSpeexRecorded appendBytes:data length:sizeof(SInt16)*numFrames*numChannels];
                 
                 NSMutableDictionary *info =[NSMutableDictionary dictionaryWithCapacity:1];
                 
                 [info setObject:self.testDataSpeexRecorded  forKey:@"BIZ_AUDIO"];
                 
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"BIZ_NOTIFY_AUDIOSEND" object:nil userInfo:info];
                 
                 
                 [self.testDataSpeexRecorded setLength:0];
                 
                 [recordlockObject unlock];
             }
         }
     }];
    
    [[Novocaine instance] setInputBlock:^(float *data, UInt32 numFrames, UInt32 numChannels)
     {
         @autoreleasepool {
             
         }
     }];
    
}

-(void) muteSpeexTalk {
    
    //[[Novocaine instance] setInputSInt16Block:^(SInt16 *data, UInt32 numFrames, UInt32 numChannels) { }];
    
    _bizTalking = NO;
    
    return;
    
}

-(void) stopSpeexTalk {
    
    typeOfRecordAudioCodec =AUDIO_UNKNOWN;
    recordSequenceNumber=0;
    
    //stop audio unit process
    dispatch_block_t block = ^() {
        
        [[Novocaine instance] pause];
        [[Novocaine instance] dispoaseAudio];
        
    };
    dispatch_async(audio_play_queue, block);
    
    [[Novocaine instance] setInputSInt16Block:^(SInt16 *data, UInt32 numFrames, UInt32 numChannels) { }];
    
    return;
}


@end
