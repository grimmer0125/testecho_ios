//
//  ViewController.h
//  TestEcho
//
//  Created by Grimmer on 2015/5/30.
//  Copyright (c) 2015年 Grimmer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "C4miAudioHandler.h"


@interface ViewController : UIViewController

@property (nonatomic, assign) C4miAudioHandler *audioHandler;


- (IBAction)testRecord:(id)sender;
- (IBAction)testRecordAndPlay:(id)sender;
- (IBAction)testPlay:(id)sender;



@end

