// Copyright (c) 2012 Alex Wiltschko
// 
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

#import <CoreFoundation/CoreFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Accelerate/Accelerate.h>

#define USING_IOS
#define USEIOSBUILTIN_AEC YES


#include <Block.h>


#ifdef __cplusplus
extern "C" {
#endif
	



OSStatus inputCallback (void						*inRefCon,
						AudioUnitRenderActionFlags	* ioActionFlags,
						const AudioTimeStamp 		* inTimeStamp,
						UInt32						inOutputBusNumber,
						UInt32						inNumberFrames,
						AudioBufferList				* ioData);

OSStatus renderCallback (void						*inRefCon,
                         AudioUnitRenderActionFlags	* ioActionFlags,
                         const AudioTimeStamp 		* inTimeStamp,
                         UInt32						inOutputBusNumber,
                         UInt32						inNumberFrames,
                         AudioBufferList				* ioData);


//#if defined (USING_IOS)
void sessionPropertyListener(void *                  inClientData,
							 AudioSessionPropertyID  inID,
							 UInt32                  inDataSize,
							 const void *            inData);

//#endif


void sessionInterruptionListener(void *inClientData, UInt32 inInterruption);

#ifdef __cplusplus
}
#endif

typedef void (^OutputBlock)(float *data, UInt32 numFrames, UInt32 numChannels);
typedef void (^InputBlock)(float *data, UInt32 numFrames, UInt32 numChannels);

typedef void (^OutputSInt16Block)(SInt16 *data, UInt32 numFrames, UInt32 numChannels);
typedef void (^InputSInt16Block)(SInt16 *data, UInt32 numFrames, UInt32 numChannels);


@interface Novocaine : NSObject //<UIAlertViewDelegate>
{
	// Audio Handling
	AudioUnit inputUnit;
    AudioUnit outputUnit;
    AudioBufferList *inputBuffer;
    
	// Session Properties
	BOOL inputAvailable;
	NSString *inputRoute;
	UInt32 numInputChannels;
	UInt32 numOutputChannels;
    Float64 samplingRate;
    BOOL isInterleaved;
    UInt32 numBytesPerSample;
    AudioStreamBasicDescription input_speakerFormat;
    AudioStreamBasicDescription output_micFormat;
	
	// Audio Processing
    OutputBlock outputBlock;
    InputBlock inputBlock;
    
    OutputSInt16Block outputSInt16Block;
    InputSInt16Block inputSInt16Block;
    
	float *inData;
    float *outData;
    
    SInt16 *inDataSInt16;
    SInt16 *outDataSInt16;
	
	BOOL playing;
    BOOL interruptedOnPlayback;
}

@property AudioUnit inputUnit;
@property AudioUnit outputUnit;
@property AudioBufferList *inputBuffer;
@property (nonatomic, copy) OutputBlock outputBlock;
@property (nonatomic, copy) InputBlock inputBlock;
@property (nonatomic, copy) OutputSInt16Block outputSInt16Block;
@property (nonatomic, copy) InputSInt16Block inputSInt16Block;
@property BOOL inputAvailable;
@property (nonatomic, retain) NSString *inputRoute;
@property UInt32 numInputChannels;
@property UInt32 numOutputChannels;
@property Float64 samplingRate;
@property BOOL isInterleaved;
@property UInt32 numBytesPerSample;
@property AudioStreamBasicDescription input_speakerFormat;
@property AudioStreamBasicDescription output_micFormat;

// @property BOOL playThroughEnabled;
@property BOOL playing;
@property BOOL interruptedOnPlayback;
@property float *inData;
@property float *outData;
@property SInt16 *inDataSInt16;
@property SInt16 *outDataSInt16;

// Singleton methods
+ (Novocaine *) instance;

// Audio Unit methods
- (void)start;
- (BOOL)pause;
//- (void)setupAudio:(AudioStreamBasicDescription)format possibleRecording:(BOOL)possibleRecord;

- (void)setupAudio:(AudioStreamBasicDescription)decodeformat encode:(AudioStreamBasicDescription)encodeformat possibleRecording:(BOOL)possibleRecord;

- (void)dispoaseAudio;
//- (void)checkAvailableThenSetup:(AudioStreamBasicDescription)format possibleRecording:(BOOL)ifRecord;
- (void)checkAvailableThenSetup:(AudioStreamBasicDescription)format encode:(AudioStreamBasicDescription)encodeformat possibleRecording:(BOOL)ifRecord;

- (void)checkSessionProperties;
- (void)checkAudioSource;

//- (void)testAudio;


@end
