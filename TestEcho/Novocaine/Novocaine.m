// Copyright (c) 2012 Alex Wiltschko
// 
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// TODO:
// Switching mic and speaker on/off
//
// HOUSEKEEPING AND NICE FEATURES:
// Disambiguate outputFormat (the AUHAL's stream format)
// More nuanced input detection on the Mac
// Route switching should work, check with iPhone
// Device switching should work, check with laptop. Read that damn book.
// Wrap logging with debug macros.
// Think about what should be public, what private.
// Ability to select non-default devices.


//ref:
//1. https://github.com/alexbw/novocaine
//2. http://atastypixel.com/blog/using-remoteio-audio-unit/
//3. https://developer.apple.com/library/ios/samplecode/auriotouch/introduction/intro.html
//4. https://code.google.com/p/pjsip-iphone-audio-driver/

//5. DefaultToSpeaker
// https://github.com/seanxiaoxiao/IOS_VoiceRecognition
// http://blog.csdn.net/gh1232008/article/details/16867905

#import "Novocaine.h"
#import <AVFoundation/AVFoundation.h>

#define kInputBus 1
#define kOutputBus 0
#define kDefaultDevice 999999

#import "TargetConditionals.h"
//#import "C4miDebugPrintf.h"

#define HBRDEBUG NO
#define HBRRedLog(format, ...)        do{ if(HBRDEBUG)      NSLog((@"[HR]"format), ##__VA_ARGS__); }while(0)


static Novocaine *audioManager = nil;

@interface Novocaine()
- (NSString *)applicationDocumentsDirectory;
@end


@implementation Novocaine
@synthesize inputUnit;
@synthesize outputUnit;
@synthesize inputBuffer;
@synthesize inputRoute, inputAvailable;
@synthesize numInputChannels, numOutputChannels;
@synthesize inputBlock, outputBlock, inputSInt16Block,outputSInt16Block;
@synthesize samplingRate;
@synthesize isInterleaved;
@synthesize numBytesPerSample;
@synthesize inData,inDataSInt16;
@synthesize outData,outDataSInt16;
@synthesize playing;
@synthesize interruptedOnPlayback;

@synthesize output_micFormat;
@synthesize input_speakerFormat;

static void CheckError(OSStatus error, const char *operation)
{
	if (error == noErr) return;
	
	char str[20]={0};
	// see if it appears to be a 4-char-code
	*(UInt32 *)(str + 1) = CFSwapInt32HostToBig(error);
	if (isprint(str[1]) && isprint(str[2]) && isprint(str[3]) && isprint(str[4])) {
		str[0] = str[5] = '\'';
		str[6] = '\0';
	} else
		// no, format it as an integer
		sprintf(str, "%d", (int)error);
    
	fprintf(stderr, "Error: %s (%s)\n", operation, str);
    
    //	exit(1);
}

#pragma mark - Singleton Methods
+ (Novocaine *) instance
{
    if(audioManager==nil)
    {
        @synchronized(self)
        {
            if (audioManager == nil) {
                audioManager = [[Novocaine alloc] init];
            }
        }
    }
    return audioManager;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (audioManager == nil) {
            audioManager = [super allocWithZone:zone];
            return audioManager;  // assignment and return on first allocation
        }
    }
    return nil; // on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)init
{
	if (self = [super init])
	{
		
		// Initialize some stuff k?
        outputBlock		= nil;
		inputBlock	= nil;
        
        outputSInt16Block = nil;
        inputSInt16Block = nil;
        
        // Initialize a float buffer to hold audio
		self.inData  = (float *)calloc(8192, sizeof(float)); // probably more than we'll need
        self.outData = (float *)calloc(8192, sizeof(float));
        self.inDataSInt16 = (SInt16*)calloc(8192, sizeof(SInt16));
        self.outDataSInt16 = (SInt16*)calloc(8192, sizeof(SInt16));
        
        self.inputBlock = nil;
        self.outputBlock = nil;
        self.inputSInt16Block=nil;
        self.outputSInt16Block = nil;

        self.playing = NO;
        self.interruptedOnPlayback = NO;
		
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioSessionDidChangeInterruptionType:) name:AVAudioSessionInterruptionNotification object:[AVAudioSession sharedInstance]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleRouteChange:)
                                                     name:AVAudioSessionRouteChangeNotification
                                                   object:[AVAudioSession sharedInstance]];
        
		// Fire up the audio session ( with steady error checking ... )
//        [self checkAvailableThenSetup];
        
		return self;
		
	}
	
	return nil;
}


//#pragma mark - Block Handling
//- (void)setInputBlock:(InputBlock)newInputBlock
//{
//    InputBlock tmpBlock = inputBlock;
//    inputBlock = Block_copy(newInputBlock);
//    Block_release(tmpBlock);
//}
//
//- (void)setOutputBlock:(OutputBlock)newOutputBlock
//{
//    OutputBlock tmpBlock = outputBlock;
//    outputBlock = Block_copy(newOutputBlock);
//    Block_release(tmpBlock);
//}
//
//- (void)setInputSInt16Block:(InputSInt16Block)newInputSInt16Block
//{
//    InputSInt16Block tmpBlock = inputSInt16Block;
//    inputSInt16Block = Block_copy(newInputSInt16Block);
//    Block_release(tmpBlock);
//}
//
//- (void)setOutputSInt16Block:(OutputSInt16Block)newOutputSInt16Block
//{
//    OutputSInt16Block tmpBlock = outputSInt16Block;
//    outputSInt16Block = Block_copy(newOutputSInt16Block);
//    Block_release(tmpBlock);
//}

#pragma mark - Audio Methods

- (void)dispoaseAudio
{
    if(inputAvailable && inputUnit != NULL)
    {
        AudioUnitUninitialize(inputUnit);

        AudioComponentInstanceDispose(inputUnit);
		
        inputUnit = NULL;
    }
}

- (void)checkAvailableThenSetup:(AudioStreamBasicDescription)format encode:(AudioStreamBasicDescription)encodeformat possibleRecording:(BOOL)ifRecord
{
	// Initialize and configure the audio session, and add an interuption listener
    
//    CheckError( AudioSessionInitialize(NULL, NULL, sessionInterruptionListener, (__bridge void *)(self)), "Couldn't initialize audio session");

    [self checkAudioSource];
    
    // If we do have input, then let's rock 'n roll.
	if (self.inputAvailable) {
		[self setupAudio:format encode:encodeformat possibleRecording:ifRecord];
	}
    
    // If we don't have input, then ask the user to provide some
	else
    {
        NSLog(@"No Audio Input, Couldn't find any audio input. Plug in your Apple headphones or another microphone.!!!!");
        
//		UIAlertView *noInputAlert =
//		[[UIAlertView alloc] initWithTitle:@"No Audio Input"
//								   message:@"Couldn't find any audio input. Plug in your Apple headphones or another microphone."
//								  delegate:nil
//						 cancelButtonTitle:NSLocalizedString(@"OK",nil)
//						 otherButtonTitles:nil];
//		
//		[noInputAlert show];
	}
}

//- (void)testAudio
//{
//    UInt32 testtest4 = kAudioSessionCategory_SoloAmbientSound; //PCM, Only play default
//    
//    //        UInt32 sessionCategory = kAudioSessionCategory_PlayAndRecord;
//    CheckError( AudioSessionSetProperty (kAudioSessionProperty_AudioCategory,
//                                         sizeof (testtest4),
//                                         &testtest4), "Couldn't set audio category");
//}

- (void)setupAudio:(AudioStreamBasicDescription)decodeformat encode:(AudioStreamBasicDescription)encodeformat possibleRecording:(BOOL)possibleRecord
{
    
    // --- Audio Session Setup ---	
    
    // Add a property listener, to listen to changes to the session
//    CheckError( AudioSessionAddPropertyListener(kAudioSessionProperty_AudioRouteChange, sessionPropertyListener, (__bridge void *)(self)), "Couldn't add audio session property listener");
    
    // Set the buffer size, this will affect the number of samples that get rendered every time the audio callback is fired
    // A small number will get you lower latency audio, but will make your processor work harder
    
    //aurioTouch有, 但pjsip driver沒有
//#if !TARGET_IPHONE_SIMULATOR
//    Float32 preferredBufferSize = 0.01;//232;
//    CheckError( AudioSessionSetProperty(kAudioSessionProperty_PreferredHardwareIOBufferDuration, sizeof(preferredBufferSize), &preferredBufferSize), "Couldn't set the preferred buffer duration");
//#endif
    
    // Set the audio session active
//    CheckError( AudioSessionSetActive(YES), "Couldn't activate the audio session");
    
//    [self checkSessionProperties];
    
    // error code:10865, can not write
    
    // --- Audio Session Setup ---

    AVAudioSession *sessionInstance = [AVAudioSession sharedInstance];

    //AVAudioSessionCategoryOptionAllowBluetooth,AVAudioSessionCategoryOptionDuckOthers, not tested, 20141027
    if(possibleRecord)
    {
//        UInt32 sessionCategory = kAudioSessionCategory_PlayAndRecord;
//        
//        CheckError( AudioSessionSetProperty (kAudioSessionProperty_AudioCategory,
//                                         sizeof (sessionCategory),
//                                         &sessionCategory), "Couldn't set audio category");
        
        [sessionInstance setCategory:AVAudioSessionCategoryPlayAndRecord
                         withOptions:AVAudioSessionCategoryOptionDuckOthers | AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionDefaultToSpeaker
                               error:NULL];
    }
    else
    {
        [sessionInstance setCategory:AVAudioSessionCategorySoloAmbient
                         withOptions:AVAudioSessionCategoryOptionDuckOthers | AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionDefaultToSpeaker
                               error:NULL];
        
//        UInt32 sessionCategory = kAudioSessionCategory_SoloAmbientSound; //PCM, Only play default
//
//        CheckError( AudioSessionSetProperty (kAudioSessionProperty_AudioCategory,
//                                             sizeof (sessionCategory),
//                                             &sessionCategory), "Couldn't set audio category");
    }
    
    
    
    
     [sessionInstance overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
//    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
//    AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute,
//                             sizeof(audioRouteOverride),&audioRouteOverride);
    
//    UInt32 doChangeDefaultRoute = 1;
//    AudioSessionSetProperty ( kAudioSessionProperty_OverrideCategoryDefaultToSpeaker,
//                             sizeof (doChangeDefaultRoute), &doChangeDefaultRoute
//                             );
    
    // ----- Audio Unit Setup -----
    // ----------------------------
    
    // Describe the output unit.
    AudioComponentDescription inputDescription = {0};
    inputDescription.componentType = kAudioUnitType_Output  ;
    inputDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    
    if(possibleRecord && USEIOSBUILTIN_AEC)
    {
        inputDescription.componentSubType = kAudioUnitSubType_VoiceProcessingIO;
    }
    else
    {
        inputDescription.componentSubType = kAudioUnitSubType_RemoteIO;
    }
        
    // Get component
    AudioComponent inputComponent = AudioComponentFindNext(NULL, &inputDescription);
    CheckError( AudioComponentInstanceNew(inputComponent, &inputUnit), "Couldn't create the output audio unit");
    
//    NSLog(@"input:%@",&inputUnit);
    
    // Enable input
    //    kAudioOutputUnitProperty_EnableIO, for enabling or disabling input or output on an I/O unit. By default, output is enabled but input is disabled.
    UInt32 one = 1;
    if (possibleRecord)
    {
        CheckError( AudioUnitSetProperty(inputUnit,
                                         kAudioOutputUnitProperty_EnableIO,
                                         kAudioUnitScope_Input, //
                                         kInputBus,
                                         &one,
                                         sizeof(one)), "Couldn't enable IO on the input scope of output unit");
    }

    //step: read the default hardware parameters and append/change some parameters, then apply them.
    UInt32 size =  sizeof(AudioStreamBasicDescription);
	CheckError( AudioUnitGetProperty( inputUnit,
                                     kAudioUnitProperty_StreamFormat, 
                                     kAudioUnitScope_Input, 
                                     kOutputBus, //nova原是寫1, 
                                     &input_speakerFormat, 
                                     &size),
               "Couldn't get the hardware input stream format"); //喇叭
    
    
    //4142,代表 1 frame = 1 packet = 4 byte for 1 channel
    HBRRedLog(@"status of default speaker format:%f,\n%u,\n%u,\n%u,\n%u,\n%u,\n%u,\n%u,\n%u",
              input_speakerFormat.mSampleRate,
              (unsigned int)input_speakerFormat.mFormatID, //? //1819304813
              (unsigned int)input_speakerFormat.mBytesPerPacket,// ? //2
              (unsigned int)input_speakerFormat.mFramesPerPacket,//? 1
              (unsigned int)input_speakerFormat.mBytesPerFrame, //? shuld 2
              (unsigned int)input_speakerFormat.mChannelsPerFrame, //1
              (unsigned int)input_speakerFormat.mBitsPerChannel, //16
              (unsigned int)input_speakerFormat.mFormatFlags, //12 ???
              (unsigned int)input_speakerFormat.mReserved); //0

      //喇叭
//    inputFormat.mFormatID= kAudioFormatLinearPCM;//1819304813;
//    inputFormat.mBytesPerPacket= 2;//4;
//    inputFormat.mFramesPerPacket=1;
//    inputFormat.mBytesPerFrame= 2;//4;
//    inputFormat.mChannelsPerFrame=1; //之前ipad mini 是2?????
//    inputFormat.mBitsPerChannel=16;//32; //
//    inputFormat.mFormatFlags= 12;//outputFormat.mFormatFlags;//kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
//    inputFormat.mReserved=0;
//    inputFormat.mFormatFlags = kAudioFormatFlagsNativeEndian | kAudioFormatFlagIsPacked |
//    kAudioFormatFlagIsFloat;
//    inputFormat.mChannelsPerFrame=1;
//    inputFormat.mChannelsPerFrame=2;
    
    //setup
    //input,output .numBytesPerSample shoulbe be the same
    input_speakerFormat.mSampleRate = decodeformat.mSampleRate; //44100;
    input_speakerFormat.mBytesPerFrame= decodeformat.mBytesPerFrame; //2 or 4 
    
    input_speakerFormat.mChannelsPerFrame = 1;//format.mChannelsPerFrame; //mono or stereo channel
    input_speakerFormat.mBitsPerChannel= decodeformat.mBitsPerChannel; //16 or 32
    
    //this parameter should be alterd by the above parametes except sampleRate
    input_speakerFormat.mFormatFlags= decodeformat.mFormatFlags;
    
    input_speakerFormat.mBytesPerPacket = input_speakerFormat.mBytesPerFrame;

    //8000;//44100;//8000;//8000, record, render時會有error, ipad mini
    //    self.samplingRate = inputFormat.mSampleRate;

      //mic
//    outputFormat.mFormatID= kAudioFormatLinearPCM;
//    outputFormat.mBytesPerPacket= 2;//4;
//    outputFormat.mFramesPerPacket=1;
//    outputFormat.mBytesPerFrame= 2;//4;
//    outputFormat.mChannelsPerFrame=1; //之前ipad mini 是2?????
//    outputFormat.mBitsPerChannel=16;//32; //
//    outputFormat.mFormatFlags= 12;
//    outputFormat.mReserved=0;
    
//    outputFormat.mFormatFlags= kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
    
//    outputFormat.mFormatFlags = 9;
//    inputFormat.mFormatFlags = 9;
//    outputFormat.mBitsPerChannel = 16;
//    inputFormat.mFormatFlags=12;
//    inputFormat.mBitsPerChannel=16;    
//    outputFormat.mChannelsPerFrame=1;
//    inputFormat.mChannelsPerFrame =1;
        
//    inputFormat.mBytesPerPacket= 2;
//    inputFormat.mBytesPerFrame= 2;
//    outputFormat.mBytesPerPacket= 2;
//    outputFormat.mBytesPerFrame= 2;
    
//    outputFormat.mChannelsPerFrame = 1;
//    outputFormat.mChannelsPerFrame=1;
//    outputFormat.mBytesPerPacket=2;
//    outputFormat.mBytesPerFrame =2;
//    outputFormat.mBitsPerChannel=16;//改了init不了, ipadmini
    
//    outputFormat.mFormatFlags =
//    kAudioFormatFlagsNativeEndian |
//    kAudioFormatFlagIsPacked |
//    kAudioFormatFlagIsFloat |
//    kAudioFormatFlagIsNonInterleaved;

    // Configure output stream, 喇叭
    // Note: We're setting the format of the data we'll be supplying/inputting to the output stream.
    size = sizeof(AudioStreamBasicDescription);
	CheckError(AudioUnitSetProperty(inputUnit,
									kAudioUnitProperty_StreamFormat,
									kAudioUnitScope_Input,
									kOutputBus,
									&input_speakerFormat, 
									size),
			   "Couldn't set the speaker ASBD on the audio unit (after setting its sampling rate)");
        
    CheckError( AudioUnitGetProperty( inputUnit,
                                     kAudioUnitProperty_StreamFormat,
                                     kAudioUnitScope_Input,
                                     kOutputBus,
                                     &input_speakerFormat,
                                     &size ),
               "Couldn't get the speaker hardware input stream format");
    

        
    HBRRedLog(@"status of speaker format after setting:%f,\n%u,\n%u,\n%u,\n%u,\n%u,\n%u,\n%u,\n%u",
          input_speakerFormat.mSampleRate,
          (unsigned int)input_speakerFormat.mFormatID, //? //1819304813
          (unsigned int)input_speakerFormat.mBytesPerPacket,// ? //2
          (unsigned int)input_speakerFormat.mFramesPerPacket,//? 1
          (unsigned int)input_speakerFormat.mBytesPerFrame, //? shuld 2
          (unsigned int)input_speakerFormat.mChannelsPerFrame, //1
          (unsigned int)input_speakerFormat.mBitsPerChannel, //16
          (unsigned int)input_speakerFormat.mFormatFlags, //12 ???
          (unsigned int)input_speakerFormat.mReserved); //0
    
    if(possibleRecord)
    {
        // Check the output stream format, mic
        size = sizeof( AudioStreamBasicDescription );
        CheckError( AudioUnitGetProperty( inputUnit,
                                         kAudioUnitProperty_StreamFormat,
                                         kAudioUnitScope_Output,
                                         kInputBus,
                                         &output_micFormat,
                                         &size ),
                   "Couldn't get the hardware output stream format");
        
        //4142,代表 1 frame = 1 packet = 4 byte for 1 channel
        HBRRedLog(@"status of default mic format:%f,\n%u,\n%u,\n%u,\n%u,\n%u,\n%u,\n%u,\n%u",
              output_micFormat.mSampleRate,
              (unsigned int)output_micFormat.mFormatID, //? //1819304813
              (unsigned int)output_micFormat.mBytesPerPacket,// ? //2
              (unsigned int)output_micFormat.mFramesPerPacket,//? 1
              (unsigned int)output_micFormat.mBytesPerFrame, //? shuld 2
              (unsigned int)output_micFormat.mChannelsPerFrame, //1
              (unsigned int)output_micFormat.mBitsPerChannel, //16
              (unsigned int)output_micFormat.mFormatFlags, //12 ???
              (unsigned int)output_micFormat.mReserved); //0

        output_micFormat.mSampleRate = encodeformat.mSampleRate; //44100;
        output_micFormat.mBitsPerChannel= encodeformat.mBitsPerChannel; //16 or 32

        output_micFormat.mBytesPerFrame= encodeformat.mBytesPerFrame; //2 or 4
        output_micFormat.mFormatFlags= encodeformat.mFormatFlags;
        
        output_micFormat.mChannelsPerFrame = 1;
        output_micFormat.mBytesPerPacket = output_micFormat.mBytesPerFrame;
        
        size = sizeof(AudioStreamBasicDescription);
        CheckError(AudioUnitSetProperty(inputUnit,
                                        kAudioUnitProperty_StreamFormat,
                                        kAudioUnitScope_Output,
                                        kInputBus,
                                        &output_micFormat,//assume mic,speaker the same,
                                        size),
                   "Couldn't set the mic ASBD on the audio unit (after setting its sampling rate)");
        
        size = sizeof( AudioStreamBasicDescription );
        CheckError( AudioUnitGetProperty( inputUnit,
                                         kAudioUnitProperty_StreamFormat,
                                         kAudioUnitScope_Output,
                                         kInputBus,
                                         &output_micFormat,
                                         &size ),
                   "Couldn't get the mic hardware output stream format");
        
        HBRRedLog(@"status of mic format after setting:%f,\n%u,\n%u,\n%u,\n%u,\n%u,\n%u,\n%u,\n%u",
              output_micFormat.mSampleRate,
              (unsigned int)output_micFormat.mFormatID, //?
              (unsigned int)output_micFormat.mBytesPerPacket,// ? //2
              (unsigned int)output_micFormat.mFramesPerPacket,//? 1
              (unsigned int)output_micFormat.mBytesPerFrame, //? shuld 2
              (unsigned int)output_micFormat.mChannelsPerFrame, //1
              (unsigned int)output_micFormat.mBitsPerChannel, //16
              (unsigned int)output_micFormat.mFormatFlags, //12 ???
              (unsigned int)output_micFormat.mReserved);
    }
    
//  http://atastypixel.com/blog/using-remoteio-audio-unit/ 沒有
    // pjsip也沒有,
    // aurioTouch有
    UInt32 numFramesPerBuffer;
    size = sizeof(UInt32);
    CheckError(AudioUnitGetProperty(inputUnit, 
                                    kAudioUnitProperty_MaximumFramesPerSlice,
                                    kAudioUnitScope_Global,
                                    kOutputBus, 
                                    &numFramesPerBuffer, //1156
                                    &size), 
               "Couldn't get the number of frames per callback");
    
    self.numBytesPerSample = input_speakerFormat.mBitsPerChannel / 8;
    self.numOutputChannels = input_speakerFormat.mChannelsPerFrame;

    self.numInputChannels =  output_micFormat.mChannelsPerFrame;

	if (output_micFormat.mFormatFlags & kAudioFormatFlagIsNonInterleaved) {
        // The audio is non-interleaved
        printf("Not interleaved!\n");
        self.isInterleaved = NO;
        
	} else {
		printf ("Format is interleaved\n");
        self.isInterleaved = YES;
	}
    
    // Slap a render callback on the unit
    AURenderCallbackStruct callbackStruct;
    callbackStruct.inputProc = inputCallback;
    callbackStruct.inputProcRefCon = (__bridge void *)(self);
    
    CheckError( AudioUnitSetProperty(inputUnit,
                                     kAudioOutputUnitProperty_SetInputCallback,
                                     kAudioUnitScope_Global,
                                     kInputBus, //改成1,pjsip,網頁皆是. nova:0時是work的
                                     &callbackStruct, 
                                     sizeof(callbackStruct)), "Couldn't set the callback on the input unit");
    
    callbackStruct.inputProc = renderCallback;
    callbackStruct.inputProcRefCon = (__bridge void *)(self);

    CheckError( AudioUnitSetProperty(inputUnit, 
                                     kAudioUnitProperty_SetRenderCallback,  //playback audio,speaker
                                     kAudioUnitScope_Input,
                                     kOutputBus,
                                     &callbackStruct, 
                                     sizeof(callbackStruct)), 
               "Couldn't set the render callback on the input unit");
    

    [[AVAudioSession sharedInstance] setActive:YES error:NULL];
	CheckError(AudioUnitInitialize(inputUnit), "Couldn't initialize the output unit");	
}

- (BOOL)pause {
	
	if (playing) {
        CheckError( AudioOutputUnitStop(inputUnit), "Couldn't stop the output unit");
		playing = NO;
        return YES;
	}
    return NO;
}


- (void)start {
	
//	UInt32 isInputAvailable=0;
//	UInt32 size = sizeof(isInputAvailable);
//    
//	CheckError( AudioSessionGetProperty(kAudioSessionProperty_AudioInputAvailable,
//                                        &size,
//                                        &isInputAvailable), "Couldn't check if input was available");
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    self.inputAvailable = session.inputAvailable;//AVAudioSession.inputAvailable;//   inputAvailable//isInputAvailable;
    
	if ( self.inputAvailable ) {
		// Set the audio session category for simultaneous play and record
		if (!playing) {
			CheckError( AudioOutputUnitStart(inputUnit), "Couldn't start the output unit");
             
            self.playing = YES;
            
		}
	}
}

//錄
#pragma mark - Render Methods
OSStatus inputCallback   (void						*inRefCon,
                          AudioUnitRenderActionFlags	* ioActionFlags,
                          const AudioTimeStamp 		* inTimeStamp,
                          UInt32						inOutputBusNumber,
                          UInt32						inNumberFrames,
                          AudioBufferList			* ioData)
{

//    NSLog(@"start to callback, into callbackk:%d",(unsigned int)inNumberFrames);
	Novocaine *sm = (__bridge Novocaine *)inRefCon;
        
    if (!sm.playing)
        return noErr;
    if (sm.inputBlock == nil && sm.inputSInt16Block ==nil)
        return noErr;
    
    // Check the current number of channels		
    // Let's actually grab the audio
#if TARGET_IPHONE_SIMULATOR
    // this is a workaround for an issue with core audio on the simulator, //
    //  likely due to 44100 vs 48000 difference in OSX //
    if( inNumberFrames == 471 )
        inNumberFrames = 470;
#endif
      
    AudioBufferList *obufferList;
   
//    AudioBufferList *bufferList; // = (AudioBufferList *)malloc(propsize);

    if (sm->output_micFormat.mFormatFlags & kAudioFormatFlagIsNonInterleaved) {
        
        UInt32 propsize = offsetof(AudioBufferList, mBuffers[0]) + (sizeof(AudioBuffer) * sm->output_micFormat.mChannelsPerFrame);
        
        obufferList = (AudioBufferList *)malloc(propsize);
        
        //non-interleaved
        obufferList->mNumberBuffers =sm->output_micFormat.mChannelsPerFrame;

        for(UInt32 i =0; i< obufferList->mNumberBuffers ; i++)
        {                
            obufferList->mBuffers[i].mNumberChannels = 1;
            
            if (sm->output_micFormat.mBitsPerChannel==16) {
                obufferList->mBuffers[i].mDataByteSize = inNumberFrames*sizeof(SInt16);
            }
            else
            {
                obufferList->mBuffers[i].mDataByteSize = inNumberFrames*sizeof(float);
            }
            
            obufferList->mBuffers[i].mData = malloc(obufferList->mBuffers[i].mDataByteSize);
            
            memset(obufferList->mBuffers[i].mData, 0,
                   obufferList->mBuffers[i].mDataByteSize);
        }
        
    } else {
        
        UInt32 propsize = offsetof(AudioBufferList, mBuffers[0]) + (sizeof(AudioBuffer) * 1);
        
        obufferList = (AudioBufferList *)malloc(propsize);
        
        //interleaved
        obufferList->mNumberBuffers =1;
        
        //pre-malloc buffers for AudioBufferLists
        obufferList->mBuffers[0].mNumberChannels = sm->output_micFormat.mChannelsPerFrame;
        
        if (sm->output_micFormat.mBitsPerChannel==16) {
            obufferList->mBuffers[0].mDataByteSize =
            inNumberFrames*
            obufferList->mBuffers[0].mNumberChannels*
            sizeof(SInt16); 
        }
        else
        {
            obufferList->mBuffers[0].mDataByteSize =
            inNumberFrames*
            obufferList->mBuffers[0].mNumberChannels*
            sizeof(float);
        }
        
        obufferList->mBuffers[0].mData = //samples;
        malloc(obufferList->mBuffers[0].mDataByteSize);
        
        memset(obufferList->mBuffers[0].mData, 0,
               obufferList->mBuffers[0].mDataByteSize);
    }
    
    CheckError( AudioUnitRender(sm.inputUnit, ioActionFlags, inTimeStamp, inOutputBusNumber, inNumberFrames, obufferList), "Couldn't render the mic unit");
    
    // Convert the audio in something manageable
    // For Float32s ... 
    if ( sm.numBytesPerSample == 4 ) // then we've already got floats
    {                
        float zero = 0.0f;
        if ( sm->output_micFormat.mFormatFlags & kAudioFormatFlagIsNonInterleaved ) { // if the data is in separate buffers, make it interleaved
            for (int i=0; i < sm.numInputChannels; ++i) {
                
                //default ipad mini will be non-interleaved
                vDSP_vsadd((float *)obufferList->mBuffers[i].mData, 1, &zero, sm.inData+i, 
                           sm.numInputChannels, inNumberFrames);
            }
        }
        else { // if the data is already interleaved, copy it all in one happy block.
            // TODO: check mDataByteSize is proper   
            memcpy(sm.inData, (float *)obufferList->mBuffers[0].mData, sm.inputBuffer->mBuffers[0].mDataByteSize);
        }
    }
    
    // For SInt16s ... //simulator
    else if ( sm.numBytesPerSample == 2 ) // then we're dealing with SInt16's
    {
        if (sm->output_micFormat.mFormatFlags & kAudioFormatFlagIsNonInterleaved ) {
            
            if (sm.numInputChannels ==1)
            {
                memcpy(sm.inDataSInt16, (SInt16 *)obufferList->mBuffers[0].mData, obufferList->mBuffers[0].mDataByteSize);
            }
            else{
            
                for (int i=0; i < sm.numInputChannels; ++i)
                {
                    SInt16 *mData16= obufferList->mBuffers[i].mData;
                    
                    for (int j=0; j<inNumberFrames;j++)
                    {
                        sm.inDataSInt16[i+j*sm.numInputChannels] = mData16[j];
                    }                    
                }
            }
        }
        else {
            
            memcpy(sm.inDataSInt16, (SInt16 *)obufferList->mBuffers[0].mData, obufferList->mBuffers[0].mDataByteSize);
        }
    }
    
    // Now do the processing!
    if (sm.numBytesPerSample == 4) {
        sm.inputBlock(sm.inData, inNumberFrames, sm.numInputChannels);
    }
    else
    {
        sm.inputSInt16Block(sm.inDataSInt16, inNumberFrames, sm.numInputChannels);
    }

    for(UInt32 i =0; i< obufferList->mNumberBuffers ; i++)
    {
        free(obufferList->mBuffers[i].mData);
    }
    
    free(obufferList);
    
//    NSLog(@"end to callback");

    return noErr;
}

//放
OSStatus renderCallback (void						*inRefCon,
                         AudioUnitRenderActionFlags	* ioActionFlags,
                         const AudioTimeStamp 		* inTimeStamp,
                         UInt32						inOutputBusNumber,
                         UInt32						inNumberFrames,
                         AudioBufferList				* ioData)
{
//    NSLog(@"renderCallback");

    //交錯是這樣1 buffer , 若交錯可能是兩個buffer
    
	Novocaine *sm = (__bridge Novocaine *)inRefCon;    
    float zero = 0.0;
    
    for (int iBuffer=0; iBuffer < ioData->mNumberBuffers; ++iBuffer) {        
        memset(ioData->mBuffers[iBuffer].mData, 0, ioData->mBuffers[iBuffer].mDataByteSize);
    }
    
    if (!sm.playing)
        return noErr;
    if (!sm.outputBlock && !sm.outputSInt16Block)
        return noErr;

    // Collect data to render from the callbacks
    //就算speaker format設成mono channel, 而因為之前會去讀hardware default 的, 所以可能不一致sm.numOutputChannels ==2, 此時去存的地方拿我有copy成兩倍大
    
    if ( sm.numBytesPerSample == 4 )
    {
        sm.outputBlock(sm.outData, inNumberFrames,sm.numOutputChannels);
    }
    else
    {
        sm.outputSInt16Block(sm.outDataSInt16, inNumberFrames,sm.numOutputChannels);
    }
    
    // Put the rendered data into the output buffer
    // TODO: convert SInt16 ranges to float ranges.
    if ( sm.numBytesPerSample == 4 ) // then we've already got floats
    {
        //1. ioData->2 buffer (if speaker is 2 channel, sm.numOutputChannels=2)
        for (int iBuffer=0; iBuffer < ioData->mNumberBuffers; ++iBuffer) {
            
            int thisNumChannels = ioData->mBuffers[iBuffer].mNumberChannels;
            
            //若是上述case, 而但只有一塊buffer(mono channel), 所以會跳著拿
            //ipad mini試remote io, default : 4142
            for (int iChannel = 0; iChannel < thisNumChannels; ++iChannel) { 
                
                vDSP_vsadd(sm.outData+iChannel, sm.numOutputChannels, &zero, (float *)ioData->mBuffers[iBuffer].mData, thisNumChannels, inNumberFrames);
            }
        }
    }
    else if ( sm.numBytesPerSample == 2 ) // then we need to convert SInt16 -> Float (and also scale)
    {
        
        //總data: 512 frames*16 bit*2channel = 2048, 又之前存是16bit->32bit,
        //所以會變成4xxx
        
        if (ioData->mNumberBuffers==1) // && ioData->mBuffers[0].mNumberChannels==1)
        {
            //可能多個channel
            memcpy((SInt16 *)ioData->mBuffers[0].mData, sm.outDataSInt16, ioData->mBuffers[0].mDataByteSize);
        }
        else
        {
            for (int iBuffer=0; iBuffer < ioData->mNumberBuffers; ++iBuffer)
            {
//                int thisNumChannels = ioData->mBuffers[iBuffer].mNumberChannels;
                
//                for (int iChannel = 0; iChannel < thisNumChannels; ++iChannel)
//                {
                SInt16 *mData16= (SInt16 *)ioData->mBuffers[iBuffer].mData;

                for (int j=0; j<inNumberFrames; j++)
                {
                    mData16[j]= sm.outDataSInt16[iBuffer+j*ioData->mNumberBuffers];
                }
                
                    //copy data from sm.outData (source)- > ioData->mBuffers
                    //如果mNumberChannels>1, 則
                    //會soruce[0], source[3]跳著分派
//                }
            }
        }
    }
    
    return noErr;
    
}	

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//#pragma mark - Audio Session Listeners
//void sessionPropertyListener(void *                  inClientData,
//							 AudioSessionPropertyID  inID,
//							 UInt32                  inDataSize,
//							 const void *            inData){
//	
//    // Determines the reason for the route change, to ensure that it is not
//    //      because of a category change.
//    CFNumberRef routeChangeReasonRef = (CFNumberRef)CFDictionaryGetValue ((CFDictionaryRef)inData, CFSTR (kAudioSession_AudioRouteChangeKey_Reason) );
//    SInt32 routeChangeReason;
//    CFNumberGetValue (routeChangeReasonRef, kCFNumberSInt32Type, &routeChangeReason);
//    
//    if (inID == kAudioSessionProperty_AudioRouteChange && routeChangeReason != kAudioSessionRouteChangeReason_CategoryChange)
//    {
//        Novocaine *sm = (__bridge Novocaine *)inClientData;
//        [sm checkSessionProperties];
//    }
//    
//}

-(void)handleRouteChange:(NSNotification*)notification{
    AVAudioSession *session = [ AVAudioSession sharedInstance];
    NSString* seccReason = @"";
    NSInteger reason = [[[notification userInfo] objectForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    // AVAudioSessionRouteDescription* prevRoute = [[notification userInfo] objectForKey:AVAudioSessionRouteChangePreviousRouteKey];
    switch (reason) {
        case AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory:
            seccReason = @"The route changed because no suitable route is now available for the specified category.";
            break;
        case AVAudioSessionRouteChangeReasonWakeFromSleep:
            seccReason = @"The route changed when the device woke up from sleep.";
            break;
        case AVAudioSessionRouteChangeReasonOverride:
            seccReason = @"The output route was overridden by the app.";
            break;
        case AVAudioSessionRouteChangeReasonCategoryChange:
            seccReason = @"The category of the session object changed.";
            break;
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
            seccReason = @"The previous audio output path is no longer available.";
            break;
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            seccReason = @"A preferred new audio output path is now available.";
            break;
        case AVAudioSessionRouteChangeReasonUnknown:
        default:
            seccReason = @"The reason for the change is unknown.";
            break;
    }
    AVAudioSessionPortDescription *input = [[session.currentRoute.inputs count]?session.currentRoute.inputs:nil objectAtIndex:0];
    if (input.portType == AVAudioSessionPortHeadsetMic)
    {
        
    }
    
    if (reason != AVAudioSessionRouteChangeReasonCategoryChange) {
        [self checkSessionProperties];
    }
}

- (void)checkAudioSource {
    
    // Check what the incoming audio route is. not necessary, comment
//    UInt32 propertySize = sizeof(CFStringRef);
//    CFStringRef route;
//    CheckError( AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &propertySize, &route), "Couldn't check the audio route");
//    self.inputRoute = (__bridge NSString *)route;
//    CFRelease(route);
//    NSLog(@"AudioRoute: %@", self.inputRoute);
    
    
    // Check if there's input available.
    // TODO: check if checking for available input is redundant.
    //          Possibly there's a different property ID change?
    
    
    //change to obj-c version
//    UInt32 isInputAvailable = 0;
//    UInt32 size = sizeof(isInputAvailable);
//    CheckError( AudioSessionGetProperty(kAudioSessionProperty_AudioInputAvailable,
//                                        &size, 
//                                        &isInputAvailable), "Couldn't check if input is available");
    AVAudioSession *session = [AVAudioSession sharedInstance];

    self.inputAvailable = session.inputAvailable;//    self.inputAvailable = (BOOL)isInputAvailable;
    NSLog(@"Input available? %d", self.inputAvailable);
    
}


// To be run ONCE per session property change and once on initialization.
- (void)checkSessionProperties
{	
    // Check if there is input, and from where
    [self checkAudioSource]; //no use, comment, 20141027
    
    // Check the number of input channels.
    // Find the number of channels
//    UInt32 size = sizeof(self.numInputChannels);
//    UInt32 newNumChannels;
//    CheckError( AudioSessionGetProperty(kAudioSessionProperty_CurrentHardwareInputNumberChannels, &size, &newNumChannels), "Checking number of input channels");
    
    
//    NSInteger newNumChannels = [AVAudioSession sharedInstance].inputNumberOfChannels;
    
    AVAudioSession *session = [AVAudioSession sharedInstance];

    NSLog(@"We've got hardware %u input channels", (unsigned int)session.inputNumberOfChannels);
    
    // Check the number of input channels.
    // Find the number of channels
//    CheckError( AudioSessionGetProperty(kAudioSessionProperty_CurrentHardwareOutputNumberChannels, &size, &newNumChannels), "Checking number of output channels");
    NSLog(@"We've got hardware %u output channels", (unsigned int)session.outputNumberOfChannels);
    
    
    // Get the hardware sampling rate. This is settable, but here we're only reading.
//    Float64 currentSamplingRate;
//    size = sizeof(currentSamplingRate);
//    CheckError( AudioSessionGetProperty(kAudioSessionProperty_CurrentHardwareSampleRate, &size, &currentSamplingRate), "Checking hardware sampling rate");
    NSLog(@"Current hardware sampling rate: %f", session.sampleRate);
}

//void sessionInterruptionListener(void *inClientData, UInt32 inInterruption) {
//    
//	Novocaine *sm = (__bridge Novocaine *)inClientData;
//    
//	if (inInterruption == kAudioSessionBeginInterruption) {
//		NSLog(@"Begin interuption");
//        if([sm pause])
//        {
//            sm.interruptedOnPlayback = YES;
//        }
//		sm.inputAvailable = NO;
//	}
//	else if (inInterruption == kAudioSessionEndInterruption) {
//        if(sm.interruptedOnPlayback)
//        {
//            NSLog(@"End interuption");
//            sm.inputAvailable = YES;
//            [sm start];
//        }
//	}    
//}

- (void)audioSessionDidChangeInterruptionType:(NSNotification *)notification
{
    AVAudioSessionInterruptionType interruptionType = [[[notification userInfo]
                                                        objectForKey:AVAudioSessionInterruptionTypeKey] unsignedIntegerValue];
    if (AVAudioSessionInterruptionTypeBegan == interruptionType)
    {
        NSLog(@"Begin interuption");
        if([self pause])
        {
            self.interruptedOnPlayback = YES;
        }
        self.inputAvailable = NO;
    }
    else if (AVAudioSessionInterruptionTypeEnded == interruptionType)
    {
        NSLog(@"End interuption");
        
        if(self.interruptedOnPlayback)
        {
            self.inputAvailable = YES;
            [self start];
        }
    }
}

#pragma mark - Convenience Methods
- (NSString *)applicationDocumentsDirectory {
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}


@end

